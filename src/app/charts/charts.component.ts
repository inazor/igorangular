import { Component, OnInit } from '@angular/core';
import { single } from './chartData';
import { multi } from './chartData';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  data: any;
  single: any[];
  multi: any[];
  view: any[];
  showXAxis: boolean;
  showYAxis: boolean;
  showXAxisLabel: boolean;
  showYAxisLabel: boolean;
  gradient: boolean;
  showLegend: boolean;
  xAxisLabel: string;
  yAxisLabel: string;
  colorScheme: any;
  myChartObject: any;
  chart: any;
  constructor() { 
    Object.assign(this, { single });
    Object.assign(this, { multi });
  }

  ngOnInit() { 
    this.chart = {
      type: 'Gauge',
      data: [
        ['', 50],
        ['', 99]
      ],
      options: {
        width: 800,
        height: 420,
        greenFrom: 0,
        greenTo: 75,
        redFrom: 90,
        redTo: 100,
        yellowFrom: 75,
        yellowTo: 90,
        minorTicks: 5
      }
    };
     this.data = {
    datasets: [{
        data: [
            11,
            16,
            7,
            3,
            14
        ],
        backgroundColor: [
            "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB"
        ],
        label: 'My dataset'
    }],
    labels: [
        "Red",
        "Green",
        "Yellow",
        "Grey",
        "Blue"
    ]
}
    this.view = [700, 400];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = 'Country';
    this.showYAxisLabel = true;
    this.yAxisLabel = 'Population';
  
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };
  }
  // logs selected column
  onSelect(event) {
    console.log(event);
  }

}
