import { NgModule } from '@angular/core';
import { ChartsComponent } from './charts.component';
import { SharedModule } from '../shared/shared.module';
import {ChartModule} from 'primeng/chart';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GoogleChartsModule } from 'angular-google-charts';
@NgModule({
  declarations: [ChartsComponent],
  imports: [
    SharedModule,
    ChartModule,
    NgxChartsModule,
    GoogleChartsModule.forRoot(),
  ]
})
export class ChartsModule { }
