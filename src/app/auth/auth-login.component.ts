import { Component } from '@angular/core';
import { AuthService } from './.services/auth.service';
@Component({
  selector: 'auth-login',
  templateUrl: './auth-login.component.html',
})
export class AuthLoginComponent {

logUser: any = {Username:"",Password:""};
  
  constructor(private authService: AuthService) {

  }
  /*
         Description: 
         Sends stringified data to accessService which connects with the server
         Called from:
         Sign in button from app.login.component.html
         Input:
         None
         Output:
         None
     */
  logIn() {
    localStorage.setItem('Username',this.logUser.Username);
    this.authService.login(this.logUser).subscribe();
  }

} 
