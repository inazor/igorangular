import { Injectable, isDevMode} from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn;
  private baseUrl = localStorage.getItem('BaseURL');
  constructor(private router: Router, private http: HttpClient) { 
    if(localStorage.getItem('token'))
    this.loggedIn = new BehaviorSubject<boolean>(true);
    else
    this.loggedIn = new BehaviorSubject<boolean>(false);
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  /*
       Description: 
       Sends post request to the server with given body to log in
       Called from:
       src\app\login\app.login.component.ts 
        Input:
       Stringified json object
       Output:
       Observable object
   */
	login(body: any) {
    var loginUrl = this.baseUrl + 'auth/login';
    
    // if (isDevMode())
    //   loginUrl = "http://nis.mockable.io/auth/login";
    if (isDevMode()) console.log("AuthService:login:url " + loginUrl);

		return this.http.post<any>(loginUrl, body, { responseType: 'json' })
			.pipe(map(token => {
				if (isDevMode()) console.log("AuthService:login:token ");
				if (isDevMode()) console.log(token);
				localStorage.setItem('token', token.Data.Token),
				this.loggedIn.next(true);
				this.router.navigate(['base-user-list']);
			}
		));
	}

  logout() {
    this.loggedIn.next(false);
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  getPermission() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': "Bearer " + localStorage.getItem('token')
      })
    }
    return this.http.get<any[]>(this.baseUrl + 'permission',httpOptions).pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
  return throwError(err);
  }
  
  /*
       Description: 
       Sends post request to the server with given body to register
       Called from:
       src\app\User\user-register\user-register.component.ts 
       Input:
       Stringified json object
       Output:
       Observable object
   */
  register(body: any) {
    return this.http.post(this.baseUrl + 'auth/register', body);
  }
}

