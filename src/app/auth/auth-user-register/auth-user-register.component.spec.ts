import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthUserRegisterComponent } from './auth-user-register.component';

describe('AuthUserRegisterComponent', () => {
  let component: AuthUserRegisterComponent;
  let fixture: ComponentFixture<AuthUserRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthUserRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthUserRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
