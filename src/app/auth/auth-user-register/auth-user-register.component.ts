import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from '../.services/auth.service'
@Component({
  selector: 'auth-user-register',
  templateUrl: './auth-user-register.component.html',
  styleUrls: ['./auth-user-register.component.css']
})
export class AuthUserRegisterComponent implements OnInit {

  newUser: any;
  confirmPassword: string;
  constructor(private authService: AuthService, private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.newUser = {};
    this.confirmPassword = "";
  }
  /*
         Description: 
         Sends stringified data to accessService which connects with the server
         Called from:
         Register button from user-register.component.html
         Input:
         None
         Output:
         None
     */
  Submit() {
    if (this.newUser.Password === this.confirmPassword) {
      this.authService.register(this.newUser).subscribe();
      this.router.navigate(['login']);
    }
    else
      this.messageService.add({ severity: 'error', summary: 'Registration error', detail: 'Validation failed' });
  }

  /*
         Description: 
         Checks if all required inputs are filled, and unlocks the submit button
         Called from:
         Submit button in user-register.component.html
         Input:
         None
         Output:
         boolean
     */

  checkRequiredData() {
    if (this.newUser.Username != "" && this.newUser.Password != "" && this.confirmPassword != "" && this.newUser.Email != "")
      return false;
    return true;
  }
  cancel(){
    this.router.navigate(['login']);
  }
}