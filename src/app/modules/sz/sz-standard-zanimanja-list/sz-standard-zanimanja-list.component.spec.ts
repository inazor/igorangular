import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzStandardZanimanjaListComponent } from './sz-standard-zanimanja-list.component';

describe('SzStandardZanimanjaListComponent', () => {
  let component: SzStandardZanimanjaListComponent;
  let fixture: ComponentFixture<SzStandardZanimanjaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzStandardZanimanjaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzStandardZanimanjaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
