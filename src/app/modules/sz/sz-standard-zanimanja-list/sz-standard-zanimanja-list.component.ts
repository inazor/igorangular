import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-standard-zanimanja-list',
  templateUrl: './sz-standard-zanimanja-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzStandardZanimanjaListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'StandardZanimanja List', routerLink: ['/sz-standard-zanimanja-list'] }
    ]);
   }

  ngOnInit() {
  }
}