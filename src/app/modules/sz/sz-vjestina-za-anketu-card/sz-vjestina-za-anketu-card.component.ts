import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-za-anketu-card',
  templateUrl: './sz-vjestina-za-anketu-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzVjestinaZaAnketuCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzVjestinaZaAnketu List', routerLink: ['/sz-vjestina-za-anketu-list'] },
      { label: 'SzVjestinaZaAnketu Card', routerLink: ['/sz-vjestina-za-anketu-card'] }
    ]);
   }

  ngOnInit() {
  }
}