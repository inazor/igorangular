import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaZaAnketuCardComponent } from './sz-vjestina-za-anketu-card.component';

describe('SzVjestinaZaAnketuCardComponent', () => {
  let component: SzVjestinaZaAnketuCardComponent;
  let fixture: ComponentFixture<SzVjestinaZaAnketuCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaZaAnketuCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaZaAnketuCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});