import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-kp-list',
  templateUrl: './sz-znanje-kp-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzZnanjeKpListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzZnanjeKp List', routerLink: ['/sz-znanje-kp-list'] }
    ]);
   }

  ngOnInit() {
  }
}