import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeKpListComponent } from './sz-znanje-kp-list.component';

describe('SzZnanjeKpListComponent', () => {
  let component: SzZnanjeKpListComponent;
  let fixture: ComponentFixture<SzZnanjeKpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeKpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeKpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
