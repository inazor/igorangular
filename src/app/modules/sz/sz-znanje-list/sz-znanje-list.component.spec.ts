import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeListComponent } from './sz-znanje-list.component';

describe('SzZnanjeListComponent', () => {
  let component: SzZnanjeListComponent;
  let fixture: ComponentFixture<SzZnanjeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
