import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-list',
  templateUrl: './sz-znanje-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzZnanjeListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Znanja List', routerLink: ['/sz-znanje-list'] }
    ]);
   }

  ngOnInit() {
  }
}