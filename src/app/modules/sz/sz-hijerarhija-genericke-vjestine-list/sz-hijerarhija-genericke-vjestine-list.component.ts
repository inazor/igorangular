import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-genericke-vjestine-list',
  templateUrl: './sz-hijerarhija-genericke-vjestine-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzHijerarhijaGenerickeVjestineListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'GeneričkeVještine List', routerLink: ['/sz-hijerarhija-genericke-vjestine-list'] }
    ]);
   }

  ngOnInit() {
  }
}