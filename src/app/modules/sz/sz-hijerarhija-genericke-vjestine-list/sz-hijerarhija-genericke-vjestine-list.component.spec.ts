import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaGenerickeVjestineListComponent } from './sz-hijerarhija-genericke-vjestine-list.component';

describe('SzHijerarhijaGenerickeVjestineListComponent', () => {
  let component: SzHijerarhijaGenerickeVjestineListComponent;
  let fixture: ComponentFixture<SzHijerarhijaGenerickeVjestineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaGenerickeVjestineListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaGenerickeVjestineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
