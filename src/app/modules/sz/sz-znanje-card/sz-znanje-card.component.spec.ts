import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeCardComponent } from './sz-znanje-card.component';

describe('SzZnanjeCardComponent', () => {
  let component: SzZnanjeCardComponent;
  let fixture: ComponentFixture<SzZnanjeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});