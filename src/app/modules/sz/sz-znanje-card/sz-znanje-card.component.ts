import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-card',
  templateUrl: './sz-znanje-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzZnanjeCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Znanja List', routerLink: ['/sz-znanje-list'] },
      { label: 'Znanja Card', routerLink: ['/sz-znanje-card'] }
    ]);
   }

  ngOnInit() {
  }
}