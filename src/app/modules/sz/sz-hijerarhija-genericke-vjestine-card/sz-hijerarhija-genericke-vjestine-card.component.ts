import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-genericke-vjestine-card',
  templateUrl: './sz-hijerarhija-genericke-vjestine-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzHijerarhijaGenerickeVjestineCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'GeneričkeVještine List', routerLink: ['/sz-hijerarhija-genericke-vjestine-list'] },
      { label: 'GeneričkeVještine Card', routerLink: ['/sz-hijerarhija-genericke-vjestine-card'] }
    ]);
   }

  ngOnInit() {
  }
}