import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaGenerickeVjestineCardComponent } from './sz-hijerarhija-genericke-vjestine-card.component';

describe('SzHijerarhijaGenerickeVjestineCardComponent', () => {
  let component: SzHijerarhijaGenerickeVjestineCardComponent;
  let fixture: ComponentFixture<SzHijerarhijaGenerickeVjestineCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaGenerickeVjestineCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaGenerickeVjestineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});