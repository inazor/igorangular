import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzKljucniPosaoCardComponent } from './sz-kljucni-posao-card.component';

describe('SzKljucniPosaoCardComponent', () => {
  let component: SzKljucniPosaoCardComponent;
  let fixture: ComponentFixture<SzKljucniPosaoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzKljucniPosaoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzKljucniPosaoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});