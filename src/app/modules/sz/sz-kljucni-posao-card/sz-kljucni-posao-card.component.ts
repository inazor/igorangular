import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-kljucni-posao-card',
  templateUrl: './sz-kljucni-posao-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzKljucniPosaoCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'KljučniPoslovi List', routerLink: ['/sz-kljucni-posao-list'] },
      { label: 'KljučniPoslovi Card', routerLink: ['/sz-kljucni-posao-card'] }
    ]);
   }

  ngOnInit() {
  }
}