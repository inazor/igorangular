import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaZnanjeListComponent } from './sz-hijerarhija-znanje-list.component';

describe('SzHijerarhijaZnanjeListComponent', () => {
  let component: SzHijerarhijaZnanjeListComponent;
  let fixture: ComponentFixture<SzHijerarhijaZnanjeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaZnanjeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaZnanjeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
