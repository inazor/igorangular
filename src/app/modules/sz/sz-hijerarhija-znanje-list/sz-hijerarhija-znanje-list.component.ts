import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-znanje-list',
  templateUrl: './sz-hijerarhija-znanje-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzHijerarhijaZnanjeListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'HijerarhijeZnanja List', routerLink: ['/sz-hijerarhija-znanje-list'] }
    ]);
   }

  ngOnInit() {
  }
}