import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-proces-card',
  templateUrl: './sz-hijerarhija-proces-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzHijerarhijaProcesCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Procesi List', routerLink: ['/sz-hijerarhija-proces-list'] },
      { label: 'Procesi Card', routerLink: ['/sz-hijerarhija-proces-card'] }
    ]);
   }

  ngOnInit() {
  }
}