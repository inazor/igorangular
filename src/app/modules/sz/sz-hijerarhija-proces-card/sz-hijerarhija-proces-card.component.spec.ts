import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaProcesCardComponent } from './sz-hijerarhija-proces-card.component';

describe('SzHijerarhijaProcesCardComponent', () => {
  let component: SzHijerarhijaProcesCardComponent;
  let fixture: ComponentFixture<SzHijerarhijaProcesCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaProcesCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaProcesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});