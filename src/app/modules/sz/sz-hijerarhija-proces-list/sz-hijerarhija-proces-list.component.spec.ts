import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaProcesListComponent } from './sz-hijerarhija-proces-list.component';

describe('SzHijerarhijaProcesListComponent', () => {
  let component: SzHijerarhijaProcesListComponent;
  let fixture: ComponentFixture<SzHijerarhijaProcesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaProcesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaProcesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
