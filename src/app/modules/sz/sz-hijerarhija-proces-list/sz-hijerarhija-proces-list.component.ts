import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-proces-list',
  templateUrl: './sz-hijerarhija-proces-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzHijerarhijaProcesListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Procesi List', routerLink: ['/sz-hijerarhija-proces-list'] }
    ]);
   }

  ngOnInit() {
  }
}