import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vrsta-posla-list',
  templateUrl: './sz-vrsta-posla-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzVrstaPoslaListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'VrstePosla List', routerLink: ['/sz-vrsta-posla-list'] }
    ]);
   }

  ngOnInit() {
  }
}