import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVrstaPoslaListComponent } from './sz-vrsta-posla-list.component';

describe('SzVrstaPoslaListComponent', () => {
  let component: SzVrstaPoslaListComponent;
  let fixture: ComponentFixture<SzVrstaPoslaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVrstaPoslaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVrstaPoslaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
