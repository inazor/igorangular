import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-specificne-vjestine-card',
  templateUrl: './sz-hijerarhija-specificne-vjestine-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzHijerarhijaSpecificneVjestineCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SpecifičneVještine List', routerLink: ['/sz-hijerarhija-specificne-vjestine-list'] },
      { label: 'SpecifičneVještine Card', routerLink: ['/sz-hijerarhija-specificne-vjestine-card'] }
    ]);
   }

  ngOnInit() {
  }
}