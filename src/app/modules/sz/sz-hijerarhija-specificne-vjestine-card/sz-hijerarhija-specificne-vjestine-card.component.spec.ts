import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaSpecificneVjestineCardComponent } from './sz-hijerarhija-specificne-vjestine-card.component';

describe('SzHijerarhijaSpecificneVjestineCardComponent', () => {
  let component: SzHijerarhijaSpecificneVjestineCardComponent;
  let fixture: ComponentFixture<SzHijerarhijaSpecificneVjestineCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaSpecificneVjestineCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaSpecificneVjestineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});