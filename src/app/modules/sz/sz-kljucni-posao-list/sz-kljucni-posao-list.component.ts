import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-kljucni-posao-list',
  templateUrl: './sz-kljucni-posao-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzKljucniPosaoListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'KljučniPoslovi List', routerLink: ['/sz-kljucni-posao-list'] }
    ]);
   }

  ngOnInit() {
  }
}