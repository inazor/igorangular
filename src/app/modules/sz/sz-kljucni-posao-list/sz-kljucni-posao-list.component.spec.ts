import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzKljucniPosaoListComponent } from './sz-kljucni-posao-list.component';

describe('SzKljucniPosaoListComponent', () => {
  let component: SzKljucniPosaoListComponent;
  let fixture: ComponentFixture<SzKljucniPosaoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzKljucniPosaoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzKljucniPosaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
