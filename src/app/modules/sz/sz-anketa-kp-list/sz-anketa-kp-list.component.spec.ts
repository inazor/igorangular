import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzAnketaKpListComponent } from './sz-anketa-kp-list.component';

describe('SzAnketaKpListComponent', () => {
  let component: SzAnketaKpListComponent;
  let fixture: ComponentFixture<SzAnketaKpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzAnketaKpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzAnketaKpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
