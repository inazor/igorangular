import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-anketa-kp-list',
  templateUrl: './sz-anketa-kp-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzAnketaKpListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzAnketaKp List', routerLink: ['/sz-anketa-kp-list'] }
    ]);
   }

  ngOnInit() {
  }
}