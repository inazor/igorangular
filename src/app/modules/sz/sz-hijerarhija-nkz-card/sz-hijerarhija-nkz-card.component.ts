import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-nkz-card',
  templateUrl: './sz-hijerarhija-nkz-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzHijerarhijaNkzCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'NKZ List', routerLink: ['/sz-hijerarhija-nkz-list'] },
      { label: 'NKZ Card', routerLink: ['/sz-hijerarhija-nkz-card'] }
    ]);
   }

  ngOnInit() {
  }
}