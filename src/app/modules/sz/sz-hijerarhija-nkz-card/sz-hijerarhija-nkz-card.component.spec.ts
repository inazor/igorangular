import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaNkzCardComponent } from './sz-hijerarhija-nkz-card.component';

describe('SzHijerarhijaNkzCardComponent', () => {
  let component: SzHijerarhijaNkzCardComponent;
  let fixture: ComponentFixture<SzHijerarhijaNkzCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaNkzCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaNkzCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});