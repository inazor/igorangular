import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-anketa-sz-card',
  templateUrl: './sz-anketa-sz-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzAnketaSzCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Ankete List', routerLink: ['/sz-anketa-sz-list'] },
      { label: 'Ankete Card', routerLink: ['/sz-anketa-sz-card'] }
    ]);
   }

  ngOnInit() {
  }
}