import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzAnketaSzCardComponent } from './sz-anketa-sz-card.component';

describe('SzAnketaSzCardComponent', () => {
  let component: SzAnketaSzCardComponent;
  let fixture: ComponentFixture<SzAnketaSzCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzAnketaSzCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzAnketaSzCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});