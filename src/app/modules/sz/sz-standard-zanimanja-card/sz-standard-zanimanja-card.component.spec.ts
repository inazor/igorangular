import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzStandardZanimanjaCardComponent } from './sz-standard-zanimanja-card.component';

describe('SzStandardZanimanjaCardComponent', () => {
  let component: SzStandardZanimanjaCardComponent;
  let fixture: ComponentFixture<SzStandardZanimanjaCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzStandardZanimanjaCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzStandardZanimanjaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});