import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-standard-zanimanja-card',
  templateUrl: './sz-standard-zanimanja-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzStandardZanimanjaCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'StandardZanimanja List', routerLink: ['/sz-standard-zanimanja-list'] },
      { label: 'StandardZanimanja Card', routerLink: ['/sz-standard-zanimanja-card'] }
    ]);
   }

  ngOnInit() {
  }
}