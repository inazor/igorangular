import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeZaAnketuCardComponent } from './sz-znanje-za-anketu-card.component';

describe('SzZnanjeZaAnketuCardComponent', () => {
  let component: SzZnanjeZaAnketuCardComponent;
  let fixture: ComponentFixture<SzZnanjeZaAnketuCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeZaAnketuCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeZaAnketuCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});