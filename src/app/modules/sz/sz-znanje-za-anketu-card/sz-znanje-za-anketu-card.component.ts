import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-za-anketu-card',
  templateUrl: './sz-znanje-za-anketu-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzZnanjeZaAnketuCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzZnanjeZaAnketu List', routerLink: ['/sz-znanje-za-anketu-list'] },
      { label: 'SzZnanjeZaAnketu Card', routerLink: ['/sz-znanje-za-anketu-card'] }
    ]);
   }

  ngOnInit() {
  }
}