import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-kp-card',
  templateUrl: './sz-vjestina-kp-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzVjestinaKpCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzVjestinaKp List', routerLink: ['/sz-vjestina-kp-list'] },
      { label: 'SzVjestinaKp Card', routerLink: ['/sz-vjestina-kp-card'] }
    ]);
   }

  ngOnInit() {
  }
}