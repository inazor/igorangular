import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaKpCardComponent } from './sz-vjestina-kp-card.component';

describe('SzVjestinaKpCardComponent', () => {
  let component: SzVjestinaKpCardComponent;
  let fixture: ComponentFixture<SzVjestinaKpCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaKpCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaKpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});