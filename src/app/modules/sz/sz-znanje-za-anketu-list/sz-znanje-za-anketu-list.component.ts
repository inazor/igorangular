import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-za-anketu-list',
  templateUrl: './sz-znanje-za-anketu-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzZnanjeZaAnketuListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzZnanjeZaAnketu List', routerLink: ['/sz-znanje-za-anketu-list'] }
    ]);
   }

  ngOnInit() {
  }
}