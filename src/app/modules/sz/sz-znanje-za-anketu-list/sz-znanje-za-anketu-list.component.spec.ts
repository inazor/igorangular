import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeZaAnketuListComponent } from './sz-znanje-za-anketu-list.component';

describe('SzZnanjeZaAnketuListComponent', () => {
  let component: SzZnanjeZaAnketuListComponent;
  let fixture: ComponentFixture<SzZnanjeZaAnketuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeZaAnketuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeZaAnketuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
