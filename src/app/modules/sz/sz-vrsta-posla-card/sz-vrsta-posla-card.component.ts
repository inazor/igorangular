import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vrsta-posla-card',
  templateUrl: './sz-vrsta-posla-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzVrstaPoslaCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'VrstePosla List', routerLink: ['/sz-vrsta-posla-list'] },
      { label: 'VrstePosla Card', routerLink: ['/sz-vrsta-posla-card'] }
    ]);
   }

  ngOnInit() {
  }
}