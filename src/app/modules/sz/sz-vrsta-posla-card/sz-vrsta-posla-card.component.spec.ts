import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVrstaPoslaCardComponent } from './sz-vrsta-posla-card.component';

describe('SzVrstaPoslaCardComponent', () => {
  let component: SzVrstaPoslaCardComponent;
  let fixture: ComponentFixture<SzVrstaPoslaCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVrstaPoslaCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVrstaPoslaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});