import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-specificne-vjestine-list',
  templateUrl: './sz-hijerarhija-specificne-vjestine-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzHijerarhijaSpecificneVjestineListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SpecifičneVještine List', routerLink: ['/sz-hijerarhija-specificne-vjestine-list'] }
    ]);
   }

  ngOnInit() {
  }
}