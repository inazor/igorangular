import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaSpecificneVjestineListComponent } from './sz-hijerarhija-specificne-vjestine-list.component';

describe('SzHijerarhijaSpecificneVjestineListComponent', () => {
  let component: SzHijerarhijaSpecificneVjestineListComponent;
  let fixture: ComponentFixture<SzHijerarhijaSpecificneVjestineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaSpecificneVjestineListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaSpecificneVjestineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
