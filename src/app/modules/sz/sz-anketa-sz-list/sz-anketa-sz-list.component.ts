import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-anketa-sz-list',
  templateUrl: './sz-anketa-sz-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzAnketaSzListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Ankete List', routerLink: ['/sz-anketa-sz-list'] }
    ]);
   }

  ngOnInit() {
  }
}