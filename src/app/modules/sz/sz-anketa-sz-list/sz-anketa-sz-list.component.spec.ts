import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzAnketaSzListComponent } from './sz-anketa-sz-list.component';

describe('SzAnketaSzListComponent', () => {
  let component: SzAnketaSzListComponent;
  let fixture: ComponentFixture<SzAnketaSzListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzAnketaSzListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzAnketaSzListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
