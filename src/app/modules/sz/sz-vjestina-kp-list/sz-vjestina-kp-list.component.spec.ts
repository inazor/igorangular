import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaKpListComponent } from './sz-vjestina-kp-list.component';

describe('SzVjestinaKpListComponent', () => {
  let component: SzVjestinaKpListComponent;
  let fixture: ComponentFixture<SzVjestinaKpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaKpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaKpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
