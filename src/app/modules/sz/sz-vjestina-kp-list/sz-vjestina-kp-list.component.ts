import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-kp-list',
  templateUrl: './sz-vjestina-kp-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzVjestinaKpListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzVjestinaKp List', routerLink: ['/sz-vjestina-kp-list'] }
    ]);
   }

  ngOnInit() {
  }
}