import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaNkzListComponent } from './sz-hijerarhija-nkz-list.component';

describe('SzHijerarhijaNkzListComponent', () => {
  let component: SzHijerarhijaNkzListComponent;
  let fixture: ComponentFixture<SzHijerarhijaNkzListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaNkzListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaNkzListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
