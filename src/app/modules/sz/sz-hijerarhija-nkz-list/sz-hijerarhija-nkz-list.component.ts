import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-nkz-list',
  templateUrl: './sz-hijerarhija-nkz-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzHijerarhijaNkzListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'NKZ List', routerLink: ['/sz-hijerarhija-nkz-list'] }
    ]);
   }

  ngOnInit() {
  }
}