import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzAnketaKpCardComponent } from './sz-anketa-kp-card.component';

describe('SzAnketaKpCardComponent', () => {
  let component: SzAnketaKpCardComponent;
  let fixture: ComponentFixture<SzAnketaKpCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzAnketaKpCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzAnketaKpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});