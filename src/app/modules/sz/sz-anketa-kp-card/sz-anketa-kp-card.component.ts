import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-anketa-kp-card',
  templateUrl: './sz-anketa-kp-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzAnketaKpCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzAnketaKp List', routerLink: ['/sz-anketa-kp-list'] },
      { label: 'SzAnketaKp Card', routerLink: ['/sz-anketa-kp-card'] }
    ]);
   }

  ngOnInit() {
  }
}