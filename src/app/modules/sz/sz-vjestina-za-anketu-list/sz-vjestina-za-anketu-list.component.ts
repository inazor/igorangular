import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-za-anketu-list',
  templateUrl: './sz-vjestina-za-anketu-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzVjestinaZaAnketuListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzVjestinaZaAnketu List', routerLink: ['/sz-vjestina-za-anketu-list'] }
    ]);
   }

  ngOnInit() {
  }
}