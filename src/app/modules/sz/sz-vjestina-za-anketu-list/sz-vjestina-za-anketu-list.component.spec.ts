import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaZaAnketuListComponent } from './sz-vjestina-za-anketu-list.component';

describe('SzVjestinaZaAnketuListComponent', () => {
  let component: SzVjestinaZaAnketuListComponent;
  let fixture: ComponentFixture<SzVjestinaZaAnketuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaZaAnketuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaZaAnketuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
