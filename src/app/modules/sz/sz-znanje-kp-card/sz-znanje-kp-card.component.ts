import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-znanje-kp-card',
  templateUrl: './sz-znanje-kp-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzZnanjeKpCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'SzZnanjeKp List', routerLink: ['/sz-znanje-kp-list'] },
      { label: 'SzZnanjeKp Card', routerLink: ['/sz-znanje-kp-card'] }
    ]);
   }

  ngOnInit() {
  }
}