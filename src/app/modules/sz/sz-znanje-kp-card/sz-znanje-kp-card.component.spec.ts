import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzZnanjeKpCardComponent } from './sz-znanje-kp-card.component';

describe('SzZnanjeKpCardComponent', () => {
  let component: SzZnanjeKpCardComponent;
  let fixture: ComponentFixture<SzZnanjeKpCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzZnanjeKpCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzZnanjeKpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});