import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-hijerarhija-znanje-card',
  templateUrl: './sz-hijerarhija-znanje-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzHijerarhijaZnanjeCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'HijerarhijeZnanja List', routerLink: ['/sz-hijerarhija-znanje-list'] },
      { label: 'HijerarhijeZnanja Card', routerLink: ['/sz-hijerarhija-znanje-card'] }
    ]);
   }

  ngOnInit() {
  }
}