import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzHijerarhijaZnanjeCardComponent } from './sz-hijerarhija-znanje-card.component';

describe('SzHijerarhijaZnanjeCardComponent', () => {
  let component: SzHijerarhijaZnanjeCardComponent;
  let fixture: ComponentFixture<SzHijerarhijaZnanjeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzHijerarhijaZnanjeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzHijerarhijaZnanjeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});