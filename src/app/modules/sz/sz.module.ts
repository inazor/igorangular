import { NgModule } from '@angular/core';

//module components
import { SharedModule } from '../../shared/shared.module';

//Scaffolder-ModuleModuleTs1-Start
//Scaffolder-Model-HijerarhijaProces-Start.1
import { SzHijerarhijaProcesCardComponent } from './sz-hijerarhija-proces-card/sz-hijerarhija-proces-card.component';
import { SzHijerarhijaProcesListComponent } from './sz-hijerarhija-proces-list/sz-hijerarhija-proces-list.component';
//Scaffolder-Model-HijerarhijaProces-End.1
//Scaffolder-Model-HijerarhijaNkz-Start.1
import { SzHijerarhijaNkzCardComponent } from './sz-hijerarhija-nkz-card/sz-hijerarhija-nkz-card.component';
import { SzHijerarhijaNkzListComponent } from './sz-hijerarhija-nkz-list/sz-hijerarhija-nkz-list.component';
//Scaffolder-Model-HijerarhijaNkz-End.1
//Scaffolder-Model-StandardZanimanja-Start.1
import { SzStandardZanimanjaCardComponent } from './sz-standard-zanimanja-card/sz-standard-zanimanja-card.component';
import { SzStandardZanimanjaListComponent } from './sz-standard-zanimanja-list/sz-standard-zanimanja-list.component';
//Scaffolder-Model-StandardZanimanja-End.1
//Scaffolder-Model-VrstaPosla-Start.1
import { SzVrstaPoslaCardComponent } from './sz-vrsta-posla-card/sz-vrsta-posla-card.component';
import { SzVrstaPoslaListComponent } from './sz-vrsta-posla-list/sz-vrsta-posla-list.component';
//Scaffolder-Model-VrstaPosla-End.1
//Scaffolder-Model-KljucniPosao-Start.1
import { SzKljucniPosaoCardComponent } from './sz-kljucni-posao-card/sz-kljucni-posao-card.component';
import { SzKljucniPosaoListComponent } from './sz-kljucni-posao-list/sz-kljucni-posao-list.component';
//Scaffolder-Model-KljucniPosao-End.1
//Scaffolder-Model-AnketaSz-Start.1
import { SzAnketaSzCardComponent } from './sz-anketa-sz-card/sz-anketa-sz-card.component';
import { SzAnketaSzListComponent } from './sz-anketa-sz-list/sz-anketa-sz-list.component';
//Scaffolder-Model-AnketaSz-End.1
//Scaffolder-Model-AnketaKp-Start.1
import { SzAnketaKpCardComponent } from './sz-anketa-kp-card/sz-anketa-kp-card.component';
import { SzAnketaKpListComponent } from './sz-anketa-kp-list/sz-anketa-kp-list.component';
//Scaffolder-Model-AnketaKp-End.1
//Scaffolder-Model-VjestinaKp-Start.1
import { SzVjestinaKpCardComponent } from './sz-vjestina-kp-card/sz-vjestina-kp-card.component';
import { SzVjestinaKpListComponent } from './sz-vjestina-kp-list/sz-vjestina-kp-list.component';
//Scaffolder-Model-VjestinaKp-End.1
//Scaffolder-Model-HijerarhijaSpecificneVjestine-Start.1
import { SzHijerarhijaSpecificneVjestineCardComponent } from './sz-hijerarhija-specificne-vjestine-card/sz-hijerarhija-specificne-vjestine-card.component';
import { SzHijerarhijaSpecificneVjestineListComponent } from './sz-hijerarhija-specificne-vjestine-list/sz-hijerarhija-specificne-vjestine-list.component';
//Scaffolder-Model-HijerarhijaSpecificneVjestine-End.1
//Scaffolder-Model-HijerarhijaGenerickeVjestine-Start.1
import { SzHijerarhijaGenerickeVjestineCardComponent } from './sz-hijerarhija-genericke-vjestine-card/sz-hijerarhija-genericke-vjestine-card.component';
import { SzHijerarhijaGenerickeVjestineListComponent } from './sz-hijerarhija-genericke-vjestine-list/sz-hijerarhija-genericke-vjestine-list.component';
//Scaffolder-Model-HijerarhijaGenerickeVjestine-End.1
//Scaffolder-Model-ZnanjeKp-Start.1
import { SzZnanjeKpCardComponent } from './sz-znanje-kp-card/sz-znanje-kp-card.component';
import { SzZnanjeKpListComponent } from './sz-znanje-kp-list/sz-znanje-kp-list.component';
//Scaffolder-Model-ZnanjeKp-End.1
//Scaffolder-Model-VjestinaZaAnketu-Start.1
import { SzVjestinaZaAnketuCardComponent } from './sz-vjestina-za-anketu-card/sz-vjestina-za-anketu-card.component';
import { SzVjestinaZaAnketuListComponent } from './sz-vjestina-za-anketu-list/sz-vjestina-za-anketu-list.component';
//Scaffolder-Model-VjestinaZaAnketu-End.1
//Scaffolder-Model-HijerarhijaZnanje-Start.1
import { SzHijerarhijaZnanjeCardComponent } from './sz-hijerarhija-znanje-card/sz-hijerarhija-znanje-card.component';
import { SzHijerarhijaZnanjeListComponent } from './sz-hijerarhija-znanje-list/sz-hijerarhija-znanje-list.component';
//Scaffolder-Model-HijerarhijaZnanje-End.1
//Scaffolder-Model-Znanje-Start.1
import { SzZnanjeCardComponent } from './sz-znanje-card/sz-znanje-card.component';
import { SzZnanjeListComponent } from './sz-znanje-list/sz-znanje-list.component';
//Scaffolder-Model-Znanje-End.1
//Scaffolder-Model-Vjestina-Start.1
import { SzVjestinaCardComponent } from './sz-vjestina-card/sz-vjestina-card.component';
import { SzVjestinaListComponent } from './sz-vjestina-list/sz-vjestina-list.component';
//Scaffolder-Model-Vjestina-End.1
//Scaffolder-Model-ZnanjeZaAnketu-Start.1
import { SzZnanjeZaAnketuCardComponent } from './sz-znanje-za-anketu-card/sz-znanje-za-anketu-card.component';
import { SzZnanjeZaAnketuListComponent } from './sz-znanje-za-anketu-list/sz-znanje-za-anketu-list.component';
//Scaffolder-Model-ZnanjeZaAnketu-End.1

//Scaffolder-ModuleModuleTs1-End

@NgModule({
  declarations: [


//Scaffolder-ModuleModuleTs2-Start
//Scaffolder-Model-HijerarhijaProces-Start.2
SzHijerarhijaProcesCardComponent,
SzHijerarhijaProcesListComponent,
//Scaffolder-Model-HijerarhijaProces-End.2
//Scaffolder-Model-HijerarhijaNkz-Start.2
SzHijerarhijaNkzCardComponent,
SzHijerarhijaNkzListComponent,
//Scaffolder-Model-HijerarhijaNkz-End.2
//Scaffolder-Model-StandardZanimanja-Start.2
SzStandardZanimanjaCardComponent,
SzStandardZanimanjaListComponent,
//Scaffolder-Model-StandardZanimanja-End.2
//Scaffolder-Model-VrstaPosla-Start.2
SzVrstaPoslaCardComponent,
SzVrstaPoslaListComponent,
//Scaffolder-Model-VrstaPosla-End.2
//Scaffolder-Model-KljucniPosao-Start.2
SzKljucniPosaoCardComponent,
SzKljucniPosaoListComponent,
//Scaffolder-Model-KljucniPosao-End.2
//Scaffolder-Model-AnketaSz-Start.2
SzAnketaSzCardComponent,
SzAnketaSzListComponent,
//Scaffolder-Model-AnketaSz-End.2
//Scaffolder-Model-AnketaKp-Start.2
SzAnketaKpCardComponent,
SzAnketaKpListComponent,
//Scaffolder-Model-AnketaKp-End.2
//Scaffolder-Model-VjestinaKp-Start.2
SzVjestinaKpCardComponent,
SzVjestinaKpListComponent,
//Scaffolder-Model-VjestinaKp-End.2
//Scaffolder-Model-HijerarhijaSpecificneVjestine-Start.2
SzHijerarhijaSpecificneVjestineCardComponent,
SzHijerarhijaSpecificneVjestineListComponent,
//Scaffolder-Model-HijerarhijaSpecificneVjestine-End.2
//Scaffolder-Model-HijerarhijaGenerickeVjestine-Start.2
SzHijerarhijaGenerickeVjestineCardComponent,
SzHijerarhijaGenerickeVjestineListComponent,
//Scaffolder-Model-HijerarhijaGenerickeVjestine-End.2
//Scaffolder-Model-ZnanjeKp-Start.2
SzZnanjeKpCardComponent,
SzZnanjeKpListComponent,
//Scaffolder-Model-ZnanjeKp-End.2
//Scaffolder-Model-VjestinaZaAnketu-Start.2
SzVjestinaZaAnketuCardComponent,
SzVjestinaZaAnketuListComponent,
//Scaffolder-Model-VjestinaZaAnketu-End.2
//Scaffolder-Model-HijerarhijaZnanje-Start.2
SzHijerarhijaZnanjeCardComponent,
SzHijerarhijaZnanjeListComponent,
//Scaffolder-Model-HijerarhijaZnanje-End.2
//Scaffolder-Model-Znanje-Start.2
SzZnanjeCardComponent,
SzZnanjeListComponent,
//Scaffolder-Model-Znanje-End.2
//Scaffolder-Model-Vjestina-Start.2
SzVjestinaCardComponent,
SzVjestinaListComponent,
//Scaffolder-Model-Vjestina-End.2
//Scaffolder-Model-ZnanjeZaAnketu-Start.2
SzZnanjeZaAnketuCardComponent,
SzZnanjeZaAnketuListComponent,
//Scaffolder-Model-ZnanjeZaAnketu-End.2

//Scaffolder-ModuleModuleTs2-End

	],
  imports: [
	SharedModule,
	


  ]
})
export class SzModule { }
