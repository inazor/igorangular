import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaCardComponent } from './sz-vjestina-card.component';

describe('SzVjestinaCardComponent', () => {
  let component: SzVjestinaCardComponent;
  let fixture: ComponentFixture<SzVjestinaCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});