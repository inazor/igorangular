import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-card',
  templateUrl: './sz-vjestina-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class SzVjestinaCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Vještine List', routerLink: ['/sz-vjestina-list'] },
      { label: 'Vještine Card', routerLink: ['/sz-vjestina-card'] }
    ]);
   }

  ngOnInit() {
  }
}