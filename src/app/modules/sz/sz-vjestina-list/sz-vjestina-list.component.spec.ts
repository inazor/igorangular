import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SzVjestinaListComponent } from './sz-vjestina-list.component';

describe('SzVjestinaListComponent', () => {
  let component: SzVjestinaListComponent;
  let fixture: ComponentFixture<SzVjestinaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SzVjestinaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SzVjestinaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
