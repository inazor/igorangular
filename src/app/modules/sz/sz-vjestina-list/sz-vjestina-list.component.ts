import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'sz-vjestina-list',
  templateUrl: './sz-vjestina-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class SzVjestinaListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Vještine List', routerLink: ['/sz-vjestina-list'] }
    ]);
   }

  ngOnInit() {
  }
}