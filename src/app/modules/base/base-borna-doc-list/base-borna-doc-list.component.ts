import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'base-borna-doc-list',
  templateUrl: './base-borna-doc-list.component.html',
  //styleUrls: ['./asset-installation-order-list.component.css']
})
export class BaseBornaDocListComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'BaseBornaDoc List', routerLink: ['/base-borna-doc-list'] }
    ]);
   }

  ngOnInit() {
  }
}