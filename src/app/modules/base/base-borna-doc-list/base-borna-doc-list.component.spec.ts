import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBornaDocListComponent } from './base-borna-doc-list.component';

describe('BaseBornaDocListComponent', () => {
  let component: BaseBornaDocListComponent;
  let fixture: ComponentFixture<BaseBornaDocListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseBornaDocListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBornaDocListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
