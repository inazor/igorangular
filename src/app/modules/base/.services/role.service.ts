import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Role } from '../Role';
@Injectable({
  providedIn: 'root'
})
export class RoleService {
  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': "Bearer " + localStorage.getItem('token'),
      'responseType': 'json'
    })
  };
  private cols: any[];
  private roleUrl = 'http://nis.zapto.org:8086/api/role';
  constructor(private http: HttpClient) {
    this.cols = [
      { field: 'Id', header: 'Id' },
      { field: 'Name', header: 'Name' },
      { field: 'Slug', header: 'Slug' }
    ];
  }

  getRoles() {
    return this.http.get<any>(this.roleUrl,this.httpOptions);
  }

  deleteRole(id: number) {
    return this.http.delete(this.roleUrl+"/" + id,this.httpOptions).pipe(
      tap(data => console.log(data))
    );
  }

  addRole(role: Role) {
    return this.http.post<any>(this.roleUrl, role,this.httpOptions).pipe(catchError(this.handleError));
  }

  editRole(role: Role) {
    return this.http.put<any>(this.roleUrl + "/" + role.Id, role,this.httpOptions).pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    return throwError(err.error.Messages.DataValidation);
    }
  getCols() {
    return this.cols;
  }

  getSingleRole() {
    return this.http.get<any>(this.roleUrl + "/" + localStorage.getItem('RoleId'),this.httpOptions);
  }
}
