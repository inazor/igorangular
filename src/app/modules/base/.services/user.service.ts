import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': "Bearer " + localStorage.getItem('token'),
      'responseType': 'json'
    })
  };
  private cols: any[];
  public BaseURL1 = "http://nis.zapto.org:8086/api";
  public BaseURL = "http://demo4263120.mockable.io";
  public showToast: boolean = false;
  private userUrl = this.BaseURL1 + "/user";
  constructor(private http: HttpClient) {
    this.cols = [
      { field: 'Id', header: 'Id' },
      { field: 'FirstName', header: 'First Name' },
      { field: 'LastName', header: 'Last Name' },
      { field: 'Username', header: 'Username' },
      { field: 'Email', header: 'Email' },
    ];
  }
  //required methods
  getAll() {
    return this.getUsers();
  }
  getOne() {
    return this.getSingleUser();
  }
  deleteOne(id: number) {
    return this.deleteUser(id);
  }
  addOne(entity: any) {
    return this.addUser(entity);
  }
  editOne(entity: any) {
    return this.addUser(entity);
  }

  getModel() {
    return this.http.get<any>(this.BaseURL + "/user" + "/model", this.httpOptions);
  }
  getUsers() {
    return this.http.get<any>(this.userUrl, this.httpOptions);
  }

  deleteUser(id: number) {
    return this.http.delete(this.userUrl + "/" + id, this.httpOptions).pipe(
      tap(data => console.log(data))
    );
  }

  addUser(user: any) {
    return this.http.post<any>(this.userUrl, user, this.httpOptions).pipe(catchError(this.handleError));
  }

  editUser(user: any) {
    return this.http.put<any>(this.userUrl + "/" + user.Id, user, this.httpOptions).pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    return throwError(err.error.Messages.DataValidation);
  }
  getCols() {
    return this.cols;
  }

  getSingleUser() {
    return this.http.get<any>(this.userUrl + "/" + localStorage.getItem('userId'), this.httpOptions);
  }

}
