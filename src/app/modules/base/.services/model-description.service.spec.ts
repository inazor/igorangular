import { TestBed } from '@angular/core/testing';

import { ModelDescriptionService } from './model-description.service';

describe('ModelDescriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModelDescriptionService = TestBed.get(ModelDescriptionService);
    expect(service).toBeTruthy();
  });
});
