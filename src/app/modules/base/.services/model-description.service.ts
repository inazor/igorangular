import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModelDescriptionService {
	httpOptions = {
		headers: new HttpHeaders({
			'Authorization': "Bearer " + localStorage.getItem('token'),
			'responseType': 'json'
		})
	};

	constructor(private http: HttpClient) { }


	getModelDescription() {

		var startTime = new Date();
		while( localStorage.getItem('BaseURL') === undefined ){
			var now = new Date()
			if ( (now - startTime) > 1000 )
			throw new Error("BaseURL not found in local storage");
		}
		var url = localStorage["BaseURL"] + 'model/description';

		return this.http.get<any>(url,this.httpOptions);
	}

}
