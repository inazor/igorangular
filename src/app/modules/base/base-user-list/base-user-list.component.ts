import { Component, OnInit } from '@angular/core';
import { User } from '../User';
import { UserService } from '../.services/user.service';
import { Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'base-user-list',
  templateUrl: './base-user-list.component.html',
  styleUrls: ['./base-user-list.component.css']
})
export class BaseUserListComponent implements OnInit {
  users: User[];
  cols: string[] = ['Id','FirstName','LastName','Username','Email'];

  constructor(private userService: UserService, private messageService: MessageService,
    private router: Router, private breadcrumbService: BreadcrumbService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Users' },
      { label: 'Users List', routerLink: ['/base-user-list'] }
    ]);
  }
  ngOnInit() {

  }

}
