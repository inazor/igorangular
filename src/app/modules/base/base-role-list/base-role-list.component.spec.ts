import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseRoleListComponent } from './base-role-list.component';

describe('BaseRoleListComponent', () => {
  let component: BaseRoleListComponent;
  let fixture: ComponentFixture<BaseRoleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseRoleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseRoleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
