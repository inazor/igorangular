import { Component, OnInit } from '@angular/core';
import { Role } from '../Role';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
import { RoleService } from '../.services/role.service';
@Component({
  selector: 'base-role-list',
  templateUrl: './base-role-list.component.html',
  styleUrls: ['./base-role-list.component.css']
})
export class BaseRoleListComponent implements OnInit {

  roles: Role[];
  cols: any[];

  constructor(private RoleService: RoleService,
    private router: Router, private breadcrumbService: BreadcrumbService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Roles' },
      { label: 'Roles List', routerLink: ['/base-role-list'] }
    ]);
  }

  ngOnInit() {

  }

}
