import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBornaDocCardComponent } from './base-borna-doc-card.component';

describe('BaseBornaDocCardComponent', () => {
  let component: BaseBornaDocCardComponent;
  let fixture: ComponentFixture<BaseBornaDocCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseBornaDocCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBornaDocCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});