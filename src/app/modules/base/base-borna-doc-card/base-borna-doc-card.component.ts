import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';
@Component({
  selector: 'base-borna-doc-card',
  templateUrl: './base-borna-doc-card.component.html',
  //styleUrls: ['./asset-installation-order-card.component.css']
})
export class BaseBornaDocCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'BaseBornaDoc List', routerLink: ['/base-borna-doc-list'] },
      { label: 'BaseBornaDoc Card', routerLink: ['/base-borna-doc-card'] }
    ]);
   }

  ngOnInit() {
  }
}