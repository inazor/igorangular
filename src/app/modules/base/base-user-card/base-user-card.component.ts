import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';

@Component({
  selector: 'base-user-card',
  templateUrl: './base-user-card.component.html',
  styleUrls: ['./base-user-card.component.css']
})
export class BaseUserCardComponent implements OnInit {
  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Users' },
      { label: 'User List', routerLink: ['/base-user-list'] },
      { label: 'User Card' }
    ]);
  }

  ngOnInit() {
   }

  }

