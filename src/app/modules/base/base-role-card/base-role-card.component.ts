import { Component, OnInit } from '@angular/core';
import { Role } from '../Role';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { RoleService } from '../.services/role.service';
import { BreadcrumbService } from 'src/app/shared/.services/breadcrumb.service';

@Component({
  selector: 'base-role-card',
  templateUrl: './base-role-card.component.html',
  styleUrls: ['./base-role-card.component.css']
})
export class BaseRoleCardComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService,
    private router: Router) {
      this.breadcrumbService.setItems([
        { label: 'Roles' },
        { label: 'Role List', routerLink: ['/base-role-list'] },
        { label: 'Role Card' }
      ]);
     }

  ngOnInit() {

  }

}