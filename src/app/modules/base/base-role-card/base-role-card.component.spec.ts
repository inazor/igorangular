import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseRoleCardComponent } from './base-role-card.component';

describe('BaseRoleCardComponent', () => {
  let component: BaseRoleCardComponent;
  let fixture: ComponentFixture<BaseRoleCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseRoleCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseRoleCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
