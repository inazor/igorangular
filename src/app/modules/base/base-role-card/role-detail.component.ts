import { Component, OnInit } from '@angular/core';
import { Role } from '../Role';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { RoleService } from '../.services/role.service';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.css']
})
export class RoleDetailComponent implements OnInit {
  messages;
  cols: any[];
  role: Role;
  isAdd: boolean;
  constructor(private roleService: RoleService,
    private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.messages = {};
    this.role = { Id: null, Name: "", Slug: "" };
    if (localStorage.getItem('addRole') == "false")
      this.isAdd = false;
    else
      this.isAdd = true;
    this.cols = this.roleService.getCols();
    if (!this.isAdd)
      this.roleService.getSingleRole().subscribe(role => { this.role = role.Data; console.log(this.role) });

  }

  disableColumn(column) {
    if (column === 'Id')
      return true;
    return false;
  }
  /*
      Description: 
      After receiving response with messages with what is wrong, loop in html checks for every property if it exists in messages and 
      shows the inline message
      Called from:
      role-detail.component.html parent div of p-message
      Input:
      Name of current column
      Output:
      Boolean
  */

  messageCheck(column: string) {
    if (this.messages.hasOwnProperty(column))
      return true;
    return false;
  }


  successActions(data) {
    this.role = data.Data;
    this.showToast(data);
  }

  errorAction(err) {
    this.messages = err;
  }
  showToast(data) {
    if (data.Code === 200) {
      if (this.isAdd)
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Successfully added new role' });
      else
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Role has been updated' });
    }
  }
  saveRole() {
    if (this.isAdd)
      this.roleService.addRole(this.role).subscribe(data => this.successActions(data), err => this.errorAction(err));
    else
      this.roleService.editRole(this.role).subscribe(data => this.successActions(data), err => this.errorAction(err));
  }
  cancelRoleEdit() {
    this.router.navigate(['/role-table']);
  }
}