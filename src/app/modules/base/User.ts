
export interface LogUser {
    Password: string;
    Username: string;
}

export interface User {
    Id: number;
    Password: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Username: string;
    RoleIds: number[];
}
