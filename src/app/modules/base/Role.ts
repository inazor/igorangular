export interface Role {
    Id: number;
    Name: string;
    Slug: string;
}