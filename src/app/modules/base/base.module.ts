import { NgModule } from '@angular/core';

//module components
import { BaseUserListComponent } from './base-user-list/base-user-list.component';
import { BaseUserCardComponent } from './base-user-card/base-user-card.component';

import { BaseRoleListComponent } from './base-role-list/base-role-list.component';
import { BaseRoleCardComponent } from './base-role-card/base-role-card.component';
import { SharedModule } from '../../shared/shared.module';

//Scaffolder-ModuleModuleTs1-Start
//Scaffolder-Model-BornaDoc-Start.1
import { BaseBornaDocCardComponent } from './base-borna-doc-card/base-borna-doc-card.component';
import { BaseBornaDocListComponent } from './base-borna-doc-list/base-borna-doc-list.component';
//Scaffolder-Model-BornaDoc-End.1

//Scaffolder-ModuleModuleTs1-End

@NgModule({
  declarations: [
	BaseUserListComponent,
	BaseUserCardComponent,
	
	BaseRoleListComponent,
	BaseRoleCardComponent,

//Scaffolder-ModuleModuleTs2-Start

//Scaffolder-Model-BornaDoc-Start.2
BaseBornaDocCardComponent,
BaseBornaDocListComponent,
//Scaffolder-Model-BornaDoc-End.2

//Scaffolder-ModuleModuleTs2-End

	],
  imports: [
	SharedModule,
	


  ]
})
export class BaseModule { }
