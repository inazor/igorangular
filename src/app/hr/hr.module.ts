import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CalendarModule, TabMenuModule } from 'primeng/primeng';
import { JobapplicationComponent } from './jobapplication/jobapplication.component';


@NgModule({
  declarations: [
    JobapplicationComponent    
  ],
  imports: [
    CommonModule,
	SharedModule,
	CalendarModule,
	TabMenuModule
  ],
    exports: [
  ]

  })
export class HrModule { }
