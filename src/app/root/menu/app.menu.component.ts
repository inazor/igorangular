import { Component, Input, OnInit, AfterViewInit, ViewChild, isDevMode } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem, ScrollPanel } from 'primeng/primeng';
import { AppMainComponent } from '../app.main.component';
import { AuthService } from 'src/app/auth/.services/auth.service';
@Component({
	selector: 'app-menu',
	templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit, AfterViewInit {

	@Input() reset: boolean;

	model: any[];
	@ViewChild('scrollPanel', { static: true }) layoutMenuScrollerViewChild: ScrollPanel;

	//JS OBJECT THAT HOLDS INFO ABOUT MENUS
	objMenus: any = {
		menuSettings: {
			name: "Settings",
			icon: "settings",
			children: {
				menuBaseUsers: {
					name: "Users",
					route: "/base-user-list"
				},
				menuBaseRoles: {
					name: "Roles",
					route: '/base-role-list'
				},
				menuBasePermissions: {
					name: "Permissions",
					route: "/base-permission-list"
				},
				menuBaseAppearanceSettings:{
					name: "Appearance Settings",
					children: {
						menuBaseAppearanceMenuModes: {
							name: "Menu Modes"
						},
						menuBaseAppearanceMenuThemes: {
							name: "Menu Themes"
						},
						menuBaseAppearanceMenuLayouts: {
							name: "Menu Layouts"
						},
					}
				}
			}
		},
		menuAsset: {
			name: "Asset Management",
			children: {
				menuAssetDevices: {
					name: "Devices",
					route: "/asset-device-list"
				},
				menuAssetDevicesTree: {
					name: "Devices Tree",
					route: "/asset-device-tree"
				},
				menuAssetDevicesPickList: {
					name: "Devices Pick List",
					route: "/asset-device-pick-list"
				},
				menuAssetLocations: {
					name: "Locations",
					route: "/asset-location-list"
				},
				menuAssetCharts: {
					name: "Charts",
					route: "/charts"
				},
				menuAssetDocuments: {
					name: "Documents",
					children: {
						menuAssetInventoryReceipts: {
							name: "Inventory Receipts",
							route: "/asset-inventory-receipt-list"
						},
						menuAssetActivationOrders: {
							name: "Activation Orders",
							route: "/asset-activation-order-list"
						},
						menuAssetRelocationOrders: {
							name: "Relocation Orders",
							route: "/asset-relocation-order-list"
						},
						menuAssetScrapOrders: {
							name: "Scrap Orders",
							route: "asset-scrap-order-list"
						},	
						menuAssetInventoryAdjustments: {
							name: "Inventory Adjustments",
							route: "/asset-inventory-adjustment-list"
						},						
						menuAssetInstallationOrders: {
							name: "Installation Orders",
							route: "/asset-installation-order-list"
						},						
						
					}
				},
				menuAssetMainData: {
					name: "Main Data",
					children: {
						menuAssetLocations: {
							name: "Locations",
							route: "assets-location-list"
						}					
					}
				}					
			}
		},
		menuHr: {
			name: "HR Management",
			children: {
				menuHrJobapplication: {
					name: "Job Application",
					route: "hr-job-application"
				},			
			}
		},
		menuBase: {
			name: "Module BASE",
			children: {
			}
		},
		menuAssets: {
			name: "Module ASSETS",
			children: {
			}
		},
	}


	//primeNg theme-related menus, they are added to main menu in function addNodeToModel
	menuLayouts = {
		label: 'Layout Palette', icon: 'palette',
		items: [
			{
				label: 'Flat', icon: 'format_paint',
				items: [
					{ label: 'Blue Grey - Green', icon: 'brush', command: (event) => { this.changeLayout('bluegrey'); } },
					{ label: 'Indigo - Pink', icon: 'brush', command: (event) => { this.changeLayout('indigo'); } },
					{ label: 'Pink - Amber', icon: 'brush', command: (event) => { this.changeLayout('pink'); } },
					{ label: 'Deep Purple - Orange', icon: 'brush', command: (event) => { this.changeLayout('deeppurple'); } },
					{ label: 'Blue - Amber', icon: 'brush', command: (event) => { this.changeLayout('blue'); } },
					{
						label: 'Light Blue - Blue Grey', icon: 'brush',
						command: (event) => { this.changeLayout('lightblue'); }
					},
					{ label: 'Cyan - Amber', icon: 'brush', command: (event) => { this.changeLayout('cyan'); } },
					{ label: 'Teal - Red', icon: 'brush', command: (event) => { this.changeLayout('teal'); } },
					{ label: 'Green - Brown', icon: 'brush', command: (event) => { this.changeLayout('green'); } },
					{ label: 'Light Green - Purple', icon: 'brush', command: (event) => { this.changeLayout('lightgreen'); } },
					{ label: 'Lime - Blue Grey', icon: 'brush', command: (event) => { this.changeLayout('lime'); } },
					{ label: 'Yellow - Teal', icon: 'brush', command: (event) => { this.changeLayout('yellow'); } },
					{ label: 'Amber - Pink', icon: 'brush', command: (event) => { this.changeLayout('amber'); } },
					{ label: 'Orange - Indigo', icon: 'brush', command: (event) => { this.changeLayout('orange'); } },
					{ label: 'Deep Orange - Cyan', icon: 'brush', command: (event) => { this.changeLayout('deeporange'); } },
					{ label: 'Brown - Cyan', icon: 'brush', command: (event) => { this.changeLayout('brown'); } },
					{ label: 'Grey - Indigo', icon: 'brush', command: (event) => { this.changeLayout('grey'); } }
				]
			},
			{
				label: 'Special', icon: 'format_paint',
				items: [
					{ label: 'Reflection', icon: 'brush', command: (event) => { this.changeLayout('reflection'); } },
					{ label: 'Moody', icon: 'brush', command: (event) => { this.changeLayout('moody'); } },
					{ label: 'Cityscape', icon: 'brush', command: (event) => { this.changeLayout('cityscape'); } },
					{ label: 'Cloudy', icon: 'brush', command: (event) => { this.changeLayout('cloudy'); } },
					{ label: 'Storm', icon: 'brush', command: (event) => { this.changeLayout('storm'); } },
					{ label: 'Palm', icon: 'brush', command: (event) => { this.changeLayout('palm'); } },
					{ label: 'Flatiron', icon: 'brush', command: (event) => { this.changeLayout('flatiron'); } }
				]
			},
		]
	};
	menuModes = {
			label: 'Menu Modes', icon: 'settings',
			items: [
				//Those things are used only for changing color of theme
				//Probably wont be used
				//  {label: 'Horizontal Menu', icon: 'border_horizontal', command: (event) => {this.app.layoutMode = 'horizontal'; }},
				{ label: 'Light Menu', icon: 'label', command: (event) => { this.app.darkMenu = false; } },
				{ label: 'Dark Menu', icon: 'label_outline', command: (event) => { this.app.darkMenu = true; } }
			]
		}
	menuThemes = {
			label: 'Menu Themes', icon: 'brush', badge: '5',
			items: [
				{ label: 'Blue Grey - Green', icon: 'brush', command: (event) => { this.changeTheme('bluegrey'); } },
				{ label: 'Indigo - Pink', icon: 'brush', command: (event) => { this.changeTheme('indigo'); } },
				{ label: 'Pink - Amber', icon: 'brush', command: (event) => { this.changeTheme('pink'); } },
				{ label: 'Purple - Pink', icon: 'brush', command: (event) => { this.changeTheme('purple'); } },
				{ label: 'Deep Purple - Orange', icon: 'brush', command: (event) => { this.changeTheme('deeppurple'); } },
				{ label: 'Blue - Amber', icon: 'brush', command: (event) => { this.changeTheme('blue'); } },
				{ label: 'Light Blue - Blue Grey', icon: 'brush', command: (event) => { this.changeTheme('lightblue'); } },
				{ label: 'Cyan - Amber', icon: 'brush', command: (event) => { this.changeTheme('cyan'); } },
				{ label: 'Teal - Red', icon: 'brush', command: (event) => { this.changeTheme('teal'); } },
				{ label: 'Green - Brown', icon: 'brush', command: (event) => { this.changeTheme('green'); } },
				{ label: 'Light Green - Purple', icon: 'brush', command: (event) => { this.changeTheme('lightgreen'); } },
				{ label: 'Lime - Blue Grey', icon: 'brush', command: (event) => { this.changeTheme('lime'); } },
				{ label: 'Yellow - Teal', icon: 'brush', command: (event) => { this.changeTheme('yellow'); } },
				{ label: 'Amber - Pink', icon: 'brush', command: (event) => { this.changeTheme('amber'); } },
				{ label: 'Orange - Indigo', icon: 'brush', command: (event) => { this.changeTheme('orange'); } },
				{ label: 'Deep Orange - Cyan', icon: 'brush', command: (event) => { this.changeTheme('deeporange'); } },
				{ label: 'Brown - Cyan', icon: 'brush', command: (event) => { this.changeTheme('brown'); } },
				{ label: 'Grey - Indigo', icon: 'brush', command: (event) => { this.changeTheme('grey'); } }
			]
		}

	constructor(public app: AppMainComponent, private authService: AuthService) { 
		if (isDevMode()) console.log("AppMenuComponent:constructor");

		this.objMenus.menuAsset.children.menuAssetDocuments.children["menuAssetInstallationOrders2"] = {
			name: "Installation orders2",
			route: "/asset-installation-order-list"
		};  
		
		//Scaffolder-AppMenuComponentTs-Start

//Scaffolder-Model-BornaDoc-Start
this.objMenus.menuBase.children["menuBaseBornaDoc"] = {
	name: "BaseBornaDocs",
	route: "/base-borna-doc-list"
}; 
//Scaffolder-Model-BornaDoc-End
//Scaffolder-Module-Sz-Start
this.objMenus["menuSz"] = {
	name: "StandardZnanja",
	children:{}
}; 
//Scaffolder-Module-Sz-End
//Scaffolder-Model-HijerarhijaProces-Start
this.objMenus.menuSz.children["menuSzHijerarhijaProces"] = {
	name: "Procesi",
	route: "/sz-hijerarhija-proces-list"
}; 
//Scaffolder-Model-HijerarhijaProces-End
//Scaffolder-Model-HijerarhijaNkz-Start
this.objMenus.menuSz.children["menuSzHijerarhijaNkz"] = {
	name: "NKZ",
	route: "/sz-hijerarhija-nkz-list"
}; 
//Scaffolder-Model-HijerarhijaNkz-End
//Scaffolder-Model-StandardZanimanja-Start
this.objMenus.menuSz.children["menuSzStandardZanimanja"] = {
	name: "StandardZanimanja",
	route: "/sz-standard-zanimanja-list"
}; 
//Scaffolder-Model-StandardZanimanja-End
//Scaffolder-Model-VrstaPosla-Start
this.objMenus.menuSz.children["menuSzVrstaPosla"] = {
	name: "VrstePosla",
	route: "/sz-vrsta-posla-list"
}; 
//Scaffolder-Model-VrstaPosla-End
//Scaffolder-Model-KljucniPosao-Start
this.objMenus.menuSz.children["menuSzKljucniPosao"] = {
	name: "KljučniPoslovi",
	route: "/sz-kljucni-posao-list"
}; 
//Scaffolder-Model-KljucniPosao-End
//Scaffolder-Model-AnketaSz-Start
this.objMenus.menuSz.children["menuSzAnketaSz"] = {
	name: "Ankete",
	route: "/sz-anketa-sz-list"
}; 
//Scaffolder-Model-AnketaSz-End
//Scaffolder-Model-AnketaKp-Start
this.objMenus.menuSz.children["menuSzAnketaKp"] = {
	name: "SzAnketaKps",
	route: "/sz-anketa-kp-list"
}; 
//Scaffolder-Model-AnketaKp-End
//Scaffolder-Model-VjestinaKp-Start
this.objMenus.menuSz.children["menuSzVjestinaKp"] = {
	name: "SzVjestinaKps",
	route: "/sz-vjestina-kp-list"
}; 
//Scaffolder-Model-VjestinaKp-End
//Scaffolder-Model-HijerarhijaSpecificneVjestine-Start
this.objMenus.menuSz.children["menuSzHijerarhijaSpecificneVjestine"] = {
	name: "SpecifičneVještine",
	route: "/sz-hijerarhija-specificne-vjestine-list"
}; 
//Scaffolder-Model-HijerarhijaSpecificneVjestine-End
//Scaffolder-Model-HijerarhijaGenerickeVjestine-Start
this.objMenus.menuSz.children["menuSzHijerarhijaGenerickeVjestine"] = {
	name: "GeneričkeVještine",
	route: "/sz-hijerarhija-genericke-vjestine-list"
}; 
//Scaffolder-Model-HijerarhijaGenerickeVjestine-End
//Scaffolder-Model-ZnanjeKp-Start
this.objMenus.menuSz.children["menuSzZnanjeKp"] = {
	name: "SzZnanjeKps",
	route: "/sz-znanje-kp-list"
}; 
//Scaffolder-Model-ZnanjeKp-End
//Scaffolder-Model-VjestinaZaAnketu-Start
this.objMenus.menuSz.children["menuSzVjestinaZaAnketu"] = {
	name: "SzVjestinaZaAnketus",
	route: "/sz-vjestina-za-anketu-list"
}; 
//Scaffolder-Model-VjestinaZaAnketu-End
//Scaffolder-Model-HijerarhijaZnanje-Start
this.objMenus.menuSz.children["menuSzHijerarhijaZnanje"] = {
	name: "HijerarhijeZnanja",
	route: "/sz-hijerarhija-znanje-list"
}; 
//Scaffolder-Model-HijerarhijaZnanje-End
//Scaffolder-Model-Znanje-Start
this.objMenus.menuSz.children["menuSzZnanje"] = {
	name: "Znanja",
	route: "/sz-znanje-list"
}; 
//Scaffolder-Model-Znanje-End
//Scaffolder-Model-Vjestina-Start
this.objMenus.menuSz.children["menuSzVjestina"] = {
	name: "Vještine",
	route: "/sz-vjestina-list"
}; 
//Scaffolder-Model-Vjestina-End
//Scaffolder-Model-ZnanjeZaAnketu-Start
this.objMenus.menuSz.children["menuSzZnanjeZaAnketu"] = {
	name: "SzZnanjeZaAnketus",
	route: "/sz-znanje-za-anketu-list"
}; 
//Scaffolder-Model-ZnanjeZaAnketu-End

		//Scaffolder-AppMenuComponentTs-End
	}

	ngAfterViewInit() {
		setTimeout(() => { this.layoutMenuScrollerViewChild.moveBar(); }, 100);
	}

	ngOnInit() {
		this.model = [];
		// this.pushModel(["Asset Managmement","Settings"],"admin");
		//console.log(this.model);
		//this.authService.getPermission().subscribe(permissions => this.checkForMenus(permissions));
		if(sessionStorage.getItem('permissions') != null)
		this.pushObjMenus(JSON.parse(sessionStorage.getItem('permissions')));
		else
		this.authService.getPermission().subscribe( permissions => {
			sessionStorage.setItem('permissions',JSON.stringify(permissions));
			if (isDevMode()) console.log("AppMenuComponent:ngOnInit: permissions");
			if (isDevMode()) console.log(permissions);
			this.pushObjMenus(permissions);
		});
		
	}

	changeTheme(theme) {
		const themeLink: HTMLLinkElement = document.getElementById('theme-css') as HTMLLinkElement;
		themeLink.href = 'assets/theme/theme-' + theme + '.css';
	}

	changeLayout(theme) {
		const layoutLink: HTMLLinkElement = document.getElementById('layout-css') as HTMLLinkElement;
		layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';
	}


	/*      
	   pushObjMenus Description:
	   Based on data returned by permissions api shows menus that user is supposed to see.
	   
	   Called from:
		this.authService.getPermission().subscribe( permissions => this.pushObjMenus(permissions,"admin"));
		in
		ngOnInit()

	   Input: 
	   Array of strings containing menuNames specific for logged user
	   
	   Output:
	   None
		   */
	pushObjMenus(arrPermissions, role = "admin") {
		for (var key in this.objMenus) {
			//if (isDevMode()) console.log(key);
			var dataToPush = this.addNodeToModel(key, this.objMenus[key], arrPermissions, role)
			if (dataToPush !== null)
				this.model.push(dataToPush);
		}
	}
 
	//RECURSIVE FUNCTION
	addNodeToModel(key, node, arrPermissions, role) {
		//Validation
		//if node does not have property "name" - error
		if (!node.hasOwnProperty("name"))
			throw new Error("required property 'name' not found in node");

		//check for permission to show the menu, if role is not admin and arrPermissions does not have our menu, we exit
		if (!(role == "admin" || arrPermissions.includes(key))){
			if (isDevMode()) console.log("AppMenuComponent:addNodeToModel:key " + key);
			if (isDevMode()) console.log("AppMenuComponent:addNodeToModel:arrPermissions ");
			if (isDevMode()) console.log(arrPermissions);
			return null;
		}
		//Assignment
		var result = {};

		//calculation
		result["label"] = node.name;

		if (node.hasOwnProperty("route"))
			result["routerLink"] = node.route;

		if (node.hasOwnProperty("icon"))
			result["icon"] = node.icon;

		var hasChildren = (node.hasOwnProperty("children") && Object.keys(node.children).length > 0);
		if (hasChildren){
			var childrenKeys = Object.keys(node.children);
		   
				var items = [];
				for (var i = 0; i < childrenKeys.length; i++) {
					var subNodeKey = childrenKeys[i];
					var subNode = node.children[childrenKeys[i]];
					var subResult = this.addNodeToModel(subNodeKey, subNode, arrPermissions, role);
					if (subResult !== null && subResult.hasOwnProperty('label')){
						//if (isDevMode()) console.log("subresult : ");
						//if (isDevMode()) console.log(subResult);
						if (subNodeKey == "menuBaseAppearanceMenuModes")
							items.push (this.menuModes);
						else if (subNodeKey == "menuBaseAppearanceMenuLayouts")
							items.push(this.menuLayouts);
						else if (subNodeKey == "menuBaseAppearanceMenuThemes")
							items.push(this.menuThemes);
						else
							items.push(subResult);
					}	
				}
				result["items"] = items;
			}  
			return result;
		}


	// checkForMenus(arrPermissions: string[]) {
		// //If an array includes wanted menuName push it to the model displayed on topbar
		// if (arrPermissions.includes("menu_1")) {
			// this.model.push({ label: 'Dashboard', icon: 'dashboard', routerLink: ['/'] });
		// };
		// if (arrPermissions.includes("menu_2")) {
			// this.model.push({
				// label: 'Menu Modes', icon: 'settings',
				// items: [
					// //Those things are used only for changing color of theme
					// //Probably wont be used
					// //  {label: 'Horizontal Menu', icon: 'border_horizontal', command: (event) => {this.app.layoutMode = 'horizontal'; }},
					// { label: 'Light Menu', icon: 'label', command: (event) => { this.app.darkMenu = false; } },
					// { label: 'Dark Menu', icon: 'label_outline', command: (event) => { this.app.darkMenu = true; } }
				// ]
			// });
		// };
		// if (arrPermissions.includes("menu_3")) {
			// this.model.push({
				// label: 'Tables',
				// items: [
					// //To see component will open on different routerLinks go to src/app/app-routing.module.ts
					// { label: 'Users', routerLink: ['/user-table'], command: (event) => { localStorage.setItem('address', 'user') } },
					// { label: 'Devices', routerLink: ['/device-table'], command: (event) => { localStorage.setItem('address', 'device') } },
					// { label: 'Locations', routerLink: ['/location-table'] },
					// { label: 'Charts', routerLink: ['/charts'] },
					// { label: 'Register', routerLink: ['/register'] },
					// { label: 'Login Screen', routerLink: ['/login'] }
				// ]
			// });
			// this.model.push({
				// label: 'Administration',
				// items: [
					// { label: 'Users', routerLink: ['/user-table'] },
					// { label: 'Roles' }
				// ]
			// });
		// }
	// }

}

@Component({
	/* tslint:disable:component-selector */
	selector: '[app-submenu]',
	/* tslint:enable:component-selector */
	template: `
		<ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
			<li [ngClass]="{'active-menuitem': isActive(i)}" [class]="child.badgeStyleClass">
				<a [href]="child.url||'#'" (click)="itemClick($event,child,i)" *ngIf="!child.routerLink"
				   [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
				   (mouseenter)="onMouseEnter(i)" class="ripplelink">
					<i class="material-icons">{{child.icon}}</i>
					<span class="menuitem-text">{{child.label}}</span>
					<i class="material-icons layout-submenu-toggler" *ngIf="child.items">keyboard_arrow_down</i>
					<span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
				</a>

				<a (click)="itemClick($event,child,i)" *ngIf="child.routerLink"
				   [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink"
				   [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
				   (mouseenter)="onMouseEnter(i)" class="ripplelink">
					<i class="material-icons">{{child.icon}}</i>
					<span class="menuitem-text">{{child.label}}</span>
					<i class="material-icons layout-submenu-toggler" *ngIf="child.items">>keyboard_arrow_down</i>
					<span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
				</a>
				<ul app-submenu [item]="child" *ngIf="child.items && isActive(i)" [visible]="isActive(i)" [reset]="reset"
					[parentActive]="isActive(i)" [@children]="(app.isHorizontal())&&root ? isActive(i) ?
					'visible' : 'hidden' : isActive(i) ? 'visibleAnimated' : 'hiddenAnimated'"></ul>
			</li>
		</ng-template>
	`,
	animations: [
		trigger('children', [
			state('void', style({
				height: '0px'
			})),
			state('hiddenAnimated', style({
				height: '0px'
			})),
			state('visibleAnimated', style({
				height: '*'
			})),
			state('visible', style({
				height: '*',
				'z-index': 100
			})),
			state('hidden', style({
				height: '0px',
				'z-index': '*'
			})),
			transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
			transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
			transition('void => visibleAnimated, visibleAnimated => void',
				animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
		])
	]
})
export class AppSubMenuComponent {

	@Input() item: MenuItem;

	@Input() root: boolean;

	@Input() visible: boolean;

	_reset: boolean;

	_parentActive: boolean;

	activeIndex: number;

	constructor(public app: AppMainComponent, public router: Router, public location: Location, public appMenu: AppMenuComponent) { }

	itemClick(event: Event, item: MenuItem, index: number) {
		if (this.root) {
			this.app.menuHoverActive = !this.app.menuHoverActive;
			event.preventDefault();
		}

		// avoid processing disabled items
		if (item.disabled) {
			event.preventDefault();
			return true;
		}

		// activate current item and deactivate active sibling if any
		if (item.routerLink || item.items || item.command || item.url) {
			this.activeIndex = (this.activeIndex as number === index) ? -1 : index;
		}

		// execute command
		if (item.command) {
			item.command({ originalEvent: event, item });
		}

		// prevent hash change
		if (item.items || (!item.url && !item.routerLink)) {
			setTimeout(() => {
				this.appMenu.layoutMenuScrollerViewChild.moveBar();
			}, 450);

			event.preventDefault();
		}

		// hide menu
		if (!item.items) {
			if (this.app.isMobile()) {
				this.app.sidebarActive = false;
				this.app.mobileMenuActive = false;
			}

			if (this.app.isHorizontal()) {
				this.app.resetMenu = true;
			} else {
				this.app.resetMenu = false;
			}

			this.app.menuHoverActive = !this.app.menuHoverActive;
		}
	}

	onMouseEnter(index: number) {
		if (this.root && this.app.menuHoverActive && this.app.isHorizontal()
			&& !this.app.isMobile() && !this.app.isTablet()) {
			this.activeIndex = index;
		}
	}

	isActive(index: number): boolean {
		return this.activeIndex === index;
	}

	@Input() get reset(): boolean {
		return this._reset;
	}

	set reset(val: boolean) {
		this._reset = val;

		if (this._reset && (this.app.isHorizontal() || this.app.isOverlay())) {
			this.activeIndex = null;
		}
	}

	@Input() get parentActive(): boolean {
		return this._parentActive;
	}

	set parentActive(val: boolean) {
		this._parentActive = val;

		if (!this._parentActive) {
			this.activeIndex = null;
		}
	}
}
