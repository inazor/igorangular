import { Component } from '@angular/core';
import { AppMainComponent } from '../app.main.component';
import { AuthService } from 'src/app/auth/.services/auth.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {

    constructor(public app: AppMainComponent, private authService: AuthService) { }
    logout() {
        this.authService.logout();
    }
    showUsername(){
        return localStorage.getItem('Username');
    }
}
