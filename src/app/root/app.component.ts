import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NavaITProject';
  ngOnInit(){
    localStorage.setItem('BaseURL','http://nis.mockable.io/');
    //localStorage.setItem('BaseURL','http://localhost:5002/api/');
  }
}
