import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppMenuComponent, AppSubMenuComponent } from './menu/app.menu.component';
import { AppMainComponent } from './app.main.component';
import { AppTopBarComponent } from './menu/app.topbar.component';

import { AuthUserRegisterComponent } from '../auth/auth-user-register/auth-user-register.component';
import { AuthLoginComponent } from '../auth/auth-login.component';

import { MessageService } from 'primeng/api';
import { LocationStrategy, HashLocationStrategy, CommonModule } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { BreadcrumbService } from '../shared/.services/breadcrumb.service';

import { FieldParametersService } from '../shared/.services/fieldParameters.service';
import { RecordParametersService } from '../shared/.services/recordParameters.service';



//MODULES IMPORT
import { BaseModule } from '../modules/base/base.module';
import { AssetsModule } from '../modules/assets/assets.module';

//Scaffolder-AppModuleTs1-Start

//Scaffolder-Module-Sz-Start.1
import { SzModule } from '../modules/sz/sz.module';
//Scaffolder-Module-Sz-End.1
//Scaffolder-Module-Sz-Start.1
import { SzModule } from '../modules/sz/sz.module';
//Scaffolder-Module-Sz-End.1
//Scaffolder-Module-Sz-Start.1
import { SzModule } from '../modules/sz/sz.module';
//Scaffolder-Module-Sz-End.1
//Scaffolder-Module-Sz-Start.1
import { SzModule } from '../modules/sz/sz.module';
//Scaffolder-Module-Sz-End.1
//Scaffolder-Module-Sz-Start.1
import { SzModule } from '../modules/sz/sz.module';
//Scaffolder-Module-Sz-End.1

//Scaffolder-AppModuleTs1-End



import { SharedModule, ScrollPanelModule, PanelModule, ButtonModule, InputTextModule } from 'primeng/primeng';
import { ChartsModule } from '../charts/charts.module';
import { AppRoutingModule } from './app-routing.module';
import { AppBreadcrumbComponent } from '../shared/shared-breadcrumb/app.breadcrumb.component';
import { ToastModule } from 'primeng/toast';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorComponent } from './interceptor/interceptor.component';
import { HrModule } from '../hr/hr.module';
// What is a module https://angular.io/guide/architecture-modules
@NgModule({
  declarations: [
    AppComponent,
    AppSubMenuComponent,
    AppMainComponent,
    AppMenuComponent,
    AppTopBarComponent,
    AuthUserRegisterComponent,
    AuthLoginComponent,
    AppBreadcrumbComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ChartsModule,
    ButtonModule,
    SharedModule,
    InputTextModule,
    ToastModule,
    PanelModule,
    FormsModule,
    AppRoutingModule,
    ScrollPanelModule,

    //MODULES SCAFFOLDER
    BaseModule,
    AssetsModule,

    //Scaffolder-AppModuleTs2-Start
//Scaffolder-Module-Sz-Start.2
SzModule,
//Scaffolder-Module-Sz-End.2
//Scaffolder-Module-Sz-Start.2
SzModule,
//Scaffolder-Module-Sz-End.2
//Scaffolder-Module-Sz-Start.2
SzModule,
//Scaffolder-Module-Sz-End.2
//Scaffolder-Module-Sz-Start.2
SzModule,
//Scaffolder-Module-Sz-End.2
//Scaffolder-Module-Sz-Start.2
SzModule,
//Scaffolder-Module-Sz-End.2

    //Scaffolder-AppModuleTs2-End

    HrModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {provide: HTTP_INTERCEPTORS,useClass: InterceptorComponent,multi: true},
    BreadcrumbService, 
    FieldParametersService,
    RecordParametersService,
    MessageService, 
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
