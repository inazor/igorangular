import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { AppMainComponent } from './app.main.component';

import { AppErrorComponent } from '../root/app.error.component';
import { AuthUserRegisterComponent } from '../auth/auth-user-register/auth-user-register.component';
import { AuthLoginComponent } from '../auth/auth-login.component';

//imports for routes TODO: this shall be put inside each module

//base module
import { BaseUserListComponent } from '../modules/base/base-user-list/base-user-list.component';
import { BaseUserCardComponent } from '../modules/base/base-user-card/base-user-card.component';

import { BaseRoleListComponent } from '../modules/base/base-role-list/base-role-list.component';
import { BaseRoleCardComponent } from '../modules/base/base-role-card/base-role-card.component';



import { ChartsComponent } from '../charts/charts.component';

import { JobapplicationComponent } from '../hr/jobapplication/jobapplication.component';



//Scaffolder-AppRoutingModuleTs1-Start

//Scaffolder-Model-BornaDoc-Start.1
import { BaseBornaDocCardComponent } from '../modules/base/base-borna-doc-card/base-borna-doc-card.component';
import { BaseBornaDocListComponent } from '../modules/base/base-borna-doc-list/base-borna-doc-list.component';
//Scaffolder-Model-BornaDoc-End.1
//Scaffolder-Model-HijerarhijaProces-Start.1
import { SzHijerarhijaProcesCardComponent } from '../modules/sz/sz-hijerarhija-proces-card/sz-hijerarhija-proces-card.component';
import { SzHijerarhijaProcesListComponent } from '../modules/sz/sz-hijerarhija-proces-list/sz-hijerarhija-proces-list.component';
//Scaffolder-Model-HijerarhijaProces-End.1
//Scaffolder-Model-HijerarhijaNkz-Start.1
import { SzHijerarhijaNkzCardComponent } from '../modules/sz/sz-hijerarhija-nkz-card/sz-hijerarhija-nkz-card.component';
import { SzHijerarhijaNkzListComponent } from '../modules/sz/sz-hijerarhija-nkz-list/sz-hijerarhija-nkz-list.component';
//Scaffolder-Model-HijerarhijaNkz-End.1
//Scaffolder-Model-StandardZanimanja-Start.1
import { SzStandardZanimanjaCardComponent } from '../modules/sz/sz-standard-zanimanja-card/sz-standard-zanimanja-card.component';
import { SzStandardZanimanjaListComponent } from '../modules/sz/sz-standard-zanimanja-list/sz-standard-zanimanja-list.component';
//Scaffolder-Model-StandardZanimanja-End.1
//Scaffolder-Model-VrstaPosla-Start.1
import { SzVrstaPoslaCardComponent } from '../modules/sz/sz-vrsta-posla-card/sz-vrsta-posla-card.component';
import { SzVrstaPoslaListComponent } from '../modules/sz/sz-vrsta-posla-list/sz-vrsta-posla-list.component';
//Scaffolder-Model-VrstaPosla-End.1
//Scaffolder-Model-KljucniPosao-Start.1
import { SzKljucniPosaoCardComponent } from '../modules/sz/sz-kljucni-posao-card/sz-kljucni-posao-card.component';
import { SzKljucniPosaoListComponent } from '../modules/sz/sz-kljucni-posao-list/sz-kljucni-posao-list.component';
//Scaffolder-Model-KljucniPosao-End.1
//Scaffolder-Model-AnketaSz-Start.1
import { SzAnketaSzCardComponent } from '../modules/sz/sz-anketa-sz-card/sz-anketa-sz-card.component';
import { SzAnketaSzListComponent } from '../modules/sz/sz-anketa-sz-list/sz-anketa-sz-list.component';
//Scaffolder-Model-AnketaSz-End.1
//Scaffolder-Model-AnketaKp-Start.1
import { SzAnketaKpCardComponent } from '../modules/sz/sz-anketa-kp-card/sz-anketa-kp-card.component';
import { SzAnketaKpListComponent } from '../modules/sz/sz-anketa-kp-list/sz-anketa-kp-list.component';
//Scaffolder-Model-AnketaKp-End.1
//Scaffolder-Model-VjestinaKp-Start.1
import { SzVjestinaKpCardComponent } from '../modules/sz/sz-vjestina-kp-card/sz-vjestina-kp-card.component';
import { SzVjestinaKpListComponent } from '../modules/sz/sz-vjestina-kp-list/sz-vjestina-kp-list.component';
//Scaffolder-Model-VjestinaKp-End.1
//Scaffolder-Model-HijerarhijaSpecificneVjestine-Start.1
import { SzHijerarhijaSpecificneVjestineCardComponent } from '../modules/sz/sz-hijerarhija-specificne-vjestine-card/sz-hijerarhija-specificne-vjestine-card.component';
import { SzHijerarhijaSpecificneVjestineListComponent } from '../modules/sz/sz-hijerarhija-specificne-vjestine-list/sz-hijerarhija-specificne-vjestine-list.component';
//Scaffolder-Model-HijerarhijaSpecificneVjestine-End.1
//Scaffolder-Model-HijerarhijaGenerickeVjestine-Start.1
import { SzHijerarhijaGenerickeVjestineCardComponent } from '../modules/sz/sz-hijerarhija-genericke-vjestine-card/sz-hijerarhija-genericke-vjestine-card.component';
import { SzHijerarhijaGenerickeVjestineListComponent } from '../modules/sz/sz-hijerarhija-genericke-vjestine-list/sz-hijerarhija-genericke-vjestine-list.component';
//Scaffolder-Model-HijerarhijaGenerickeVjestine-End.1
//Scaffolder-Model-ZnanjeKp-Start.1
import { SzZnanjeKpCardComponent } from '../modules/sz/sz-znanje-kp-card/sz-znanje-kp-card.component';
import { SzZnanjeKpListComponent } from '../modules/sz/sz-znanje-kp-list/sz-znanje-kp-list.component';
//Scaffolder-Model-ZnanjeKp-End.1
//Scaffolder-Model-VjestinaZaAnketu-Start.1
import { SzVjestinaZaAnketuCardComponent } from '../modules/sz/sz-vjestina-za-anketu-card/sz-vjestina-za-anketu-card.component';
import { SzVjestinaZaAnketuListComponent } from '../modules/sz/sz-vjestina-za-anketu-list/sz-vjestina-za-anketu-list.component';
//Scaffolder-Model-VjestinaZaAnketu-End.1
//Scaffolder-Model-HijerarhijaZnanje-Start.1
import { SzHijerarhijaZnanjeCardComponent } from '../modules/sz/sz-hijerarhija-znanje-card/sz-hijerarhija-znanje-card.component';
import { SzHijerarhijaZnanjeListComponent } from '../modules/sz/sz-hijerarhija-znanje-list/sz-hijerarhija-znanje-list.component';
//Scaffolder-Model-HijerarhijaZnanje-End.1
//Scaffolder-Model-Znanje-Start.1
import { SzZnanjeCardComponent } from '../modules/sz/sz-znanje-card/sz-znanje-card.component';
import { SzZnanjeListComponent } from '../modules/sz/sz-znanje-list/sz-znanje-list.component';
//Scaffolder-Model-Znanje-End.1
//Scaffolder-Model-Vjestina-Start.1
import { SzVjestinaCardComponent } from '../modules/sz/sz-vjestina-card/sz-vjestina-card.component';
import { SzVjestinaListComponent } from '../modules/sz/sz-vjestina-list/sz-vjestina-list.component';
//Scaffolder-Model-Vjestina-End.1
//Scaffolder-Model-ZnanjeZaAnketu-Start.1
import { SzZnanjeZaAnketuCardComponent } from '../modules/sz/sz-znanje-za-anketu-card/sz-znanje-za-anketu-card.component';
import { SzZnanjeZaAnketuListComponent } from '../modules/sz/sz-znanje-za-anketu-list/sz-znanje-za-anketu-list.component';
//Scaffolder-Model-ZnanjeZaAnketu-End.1

//Scaffolder-AppRoutingModuleTs1-End


//Routes contain path which when typed after regular address (in this case localhost:4200/)
//will navigate to assigned component
//TODO  make routes according to modules, i.e. base/user-card,  asset/device-list
const routes: Routes = [
  {
    path: '', component: AppMainComponent, canActivate: [AuthGuard],
    children: [
		//base module
		{ path: 'base-user-list', component: BaseUserListComponent },
		{ path: 'base-user-card', component: BaseUserCardComponent },
		{ path: 'base-user-card/:id', component: BaseUserCardComponent },


		{ path: 'base-role-list', component: BaseRoleListComponent },
		{ path: 'base-role-card', component: BaseRoleCardComponent },
		{ path: 'base-role-card/:id', component: BaseRoleCardComponent },



		//Scaffolder-AppRoutingModuleTs2-Start
//Scaffolder-Model-BornaDoc-Start.2
{ path: 'base-borna-doc-card', component: BaseBornaDocCardComponent },
{ path: 'base-borna-doc-card/:id', component: BaseBornaDocCardComponent },
{ path: 'base-borna-doc-list', component: BaseBornaDocListComponent },
//Scaffolder-Model-BornaDoc-End.2
//Scaffolder-Model-HijerarhijaProces-Start.2
{ path: 'sz-hijerarhija-proces-card', component: SzHijerarhijaProcesCardComponent },
{ path: 'sz-hijerarhija-proces-card/:id', component: SzHijerarhijaProcesCardComponent },
{ path: 'sz-hijerarhija-proces-list', component: SzHijerarhijaProcesListComponent },
//Scaffolder-Model-HijerarhijaProces-End.2
//Scaffolder-Model-HijerarhijaNkz-Start.2
{ path: 'sz-hijerarhija-nkz-card', component: SzHijerarhijaNkzCardComponent },
{ path: 'sz-hijerarhija-nkz-card/:id', component: SzHijerarhijaNkzCardComponent },
{ path: 'sz-hijerarhija-nkz-list', component: SzHijerarhijaNkzListComponent },
//Scaffolder-Model-HijerarhijaNkz-End.2
//Scaffolder-Model-StandardZanimanja-Start.2
{ path: 'sz-standard-zanimanja-card', component: SzStandardZanimanjaCardComponent },
{ path: 'sz-standard-zanimanja-card/:id', component: SzStandardZanimanjaCardComponent },
{ path: 'sz-standard-zanimanja-list', component: SzStandardZanimanjaListComponent },
//Scaffolder-Model-StandardZanimanja-End.2
//Scaffolder-Model-VrstaPosla-Start.2
{ path: 'sz-vrsta-posla-card', component: SzVrstaPoslaCardComponent },
{ path: 'sz-vrsta-posla-card/:id', component: SzVrstaPoslaCardComponent },
{ path: 'sz-vrsta-posla-list', component: SzVrstaPoslaListComponent },
//Scaffolder-Model-VrstaPosla-End.2
//Scaffolder-Model-KljucniPosao-Start.2
{ path: 'sz-kljucni-posao-card', component: SzKljucniPosaoCardComponent },
{ path: 'sz-kljucni-posao-card/:id', component: SzKljucniPosaoCardComponent },
{ path: 'sz-kljucni-posao-list', component: SzKljucniPosaoListComponent },
//Scaffolder-Model-KljucniPosao-End.2
//Scaffolder-Model-AnketaSz-Start.2
{ path: 'sz-anketa-sz-card', component: SzAnketaSzCardComponent },
{ path: 'sz-anketa-sz-card/:id', component: SzAnketaSzCardComponent },
{ path: 'sz-anketa-sz-list', component: SzAnketaSzListComponent },
//Scaffolder-Model-AnketaSz-End.2
//Scaffolder-Model-AnketaKp-Start.2
{ path: 'sz-anketa-kp-card', component: SzAnketaKpCardComponent },
{ path: 'sz-anketa-kp-card/:id', component: SzAnketaKpCardComponent },
{ path: 'sz-anketa-kp-list', component: SzAnketaKpListComponent },
//Scaffolder-Model-AnketaKp-End.2
//Scaffolder-Model-VjestinaKp-Start.2
{ path: 'sz-vjestina-kp-card', component: SzVjestinaKpCardComponent },
{ path: 'sz-vjestina-kp-card/:id', component: SzVjestinaKpCardComponent },
{ path: 'sz-vjestina-kp-list', component: SzVjestinaKpListComponent },
//Scaffolder-Model-VjestinaKp-End.2
//Scaffolder-Model-HijerarhijaSpecificneVjestine-Start.2
{ path: 'sz-hijerarhija-specificne-vjestine-card', component: SzHijerarhijaSpecificneVjestineCardComponent },
{ path: 'sz-hijerarhija-specificne-vjestine-card/:id', component: SzHijerarhijaSpecificneVjestineCardComponent },
{ path: 'sz-hijerarhija-specificne-vjestine-list', component: SzHijerarhijaSpecificneVjestineListComponent },
//Scaffolder-Model-HijerarhijaSpecificneVjestine-End.2
//Scaffolder-Model-HijerarhijaGenerickeVjestine-Start.2
{ path: 'sz-hijerarhija-genericke-vjestine-card', component: SzHijerarhijaGenerickeVjestineCardComponent },
{ path: 'sz-hijerarhija-genericke-vjestine-card/:id', component: SzHijerarhijaGenerickeVjestineCardComponent },
{ path: 'sz-hijerarhija-genericke-vjestine-list', component: SzHijerarhijaGenerickeVjestineListComponent },
//Scaffolder-Model-HijerarhijaGenerickeVjestine-End.2
//Scaffolder-Model-ZnanjeKp-Start.2
{ path: 'sz-znanje-kp-card', component: SzZnanjeKpCardComponent },
{ path: 'sz-znanje-kp-card/:id', component: SzZnanjeKpCardComponent },
{ path: 'sz-znanje-kp-list', component: SzZnanjeKpListComponent },
//Scaffolder-Model-ZnanjeKp-End.2
//Scaffolder-Model-VjestinaZaAnketu-Start.2
{ path: 'sz-vjestina-za-anketu-card', component: SzVjestinaZaAnketuCardComponent },
{ path: 'sz-vjestina-za-anketu-card/:id', component: SzVjestinaZaAnketuCardComponent },
{ path: 'sz-vjestina-za-anketu-list', component: SzVjestinaZaAnketuListComponent },
//Scaffolder-Model-VjestinaZaAnketu-End.2
//Scaffolder-Model-HijerarhijaZnanje-Start.2
{ path: 'sz-hijerarhija-znanje-card', component: SzHijerarhijaZnanjeCardComponent },
{ path: 'sz-hijerarhija-znanje-card/:id', component: SzHijerarhijaZnanjeCardComponent },
{ path: 'sz-hijerarhija-znanje-list', component: SzHijerarhijaZnanjeListComponent },
//Scaffolder-Model-HijerarhijaZnanje-End.2
//Scaffolder-Model-Znanje-Start.2
{ path: 'sz-znanje-card', component: SzZnanjeCardComponent },
{ path: 'sz-znanje-card/:id', component: SzZnanjeCardComponent },
{ path: 'sz-znanje-list', component: SzZnanjeListComponent },
//Scaffolder-Model-Znanje-End.2
//Scaffolder-Model-Vjestina-Start.2
{ path: 'sz-vjestina-card', component: SzVjestinaCardComponent },
{ path: 'sz-vjestina-card/:id', component: SzVjestinaCardComponent },
{ path: 'sz-vjestina-list', component: SzVjestinaListComponent },
//Scaffolder-Model-Vjestina-End.2
//Scaffolder-Model-ZnanjeZaAnketu-Start.2
{ path: 'sz-znanje-za-anketu-card', component: SzZnanjeZaAnketuCardComponent },
{ path: 'sz-znanje-za-anketu-card/:id', component: SzZnanjeZaAnketuCardComponent },
{ path: 'sz-znanje-za-anketu-list', component: SzZnanjeZaAnketuListComponent },
//Scaffolder-Model-ZnanjeZaAnketu-End.2

		//Scaffolder-AppRoutingModuleTs2-End


		
		//HR module
		{ path: 'hr-job-application', component: JobapplicationComponent },


		{ path: '', redirectTo: 'base-department-list', pathMatch: 'full' }

		
    ]
  },
  { path: 'error', component: AppErrorComponent },
  { path: 'register', component: AuthUserRegisterComponent },
  { path: 'login', component: AuthLoginComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
