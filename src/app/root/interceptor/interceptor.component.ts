import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

@Injectable()
export class InterceptorComponent implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(tap(e => {
      if (e instanceof HttpResponse)
        if (e.body.hasOwnProperty('Messages')) {
          if (e.body.Messages.hasOwnProperty('Danger') && e.body.Messages.Danger.length > 0)
            this.messageService.add({ severity: 'error', summary: 'Http', detail: e.body.Messages.Danger });
          if (e.body.Messages.hasOwnProperty('Info') && e.body.Messages.Info.length > 0)
            this.messageService.add({ severity: 'success', summary: 'Http', detail: e.body.Messages.Info });
          if (e.body.Messages.hasOwnProperty('Warning') && e.body.Messages.Warning.length > 0)
            this.messageService.add({ severity: 'warn', summary: 'Http', detail: e.body.Messages.Warning });
          if (e.body.Messages.hasOwnProperty('DataValidation')) {
            for (let key of Object.keys(e.body.Messages.DataValidation)) {
              let dataName = e.body.Messages.DataValidation[key];
              this.messageService.add({ severity: 'error', summary: key, detail: dataName });
            }
          }
        }
    })
    )
  }
  constructor(private messageService: MessageService) { }

}
