export enum EFieldDisplayMode {
    Card, ListLabel, ListFilter, ListDataNonEditable, ListDataEditable
}
