import { Component, OnInit, Input } from '@angular/core';
import { IWidgetParameters } from '../../interfaces/widgetParameters';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetLabel',
  templateUrl: './widgetLabel.component.html',
  styleUrls: ['./widgetLabel.component.css']
})
export class WidgetLabelComponent implements OnInit {

  @Input() p : IFieldParameters;

  constructor() { }

  ngOnInit() {
  }

}
