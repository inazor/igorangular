import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetOne2Many',
  templateUrl: './widgetOne2Many.component.html',
  styleUrls: ['./widgetOne2Many.component.css']
})
export class WidgetOne2ManyComponent implements OnInit {

  
  @Input('p') fieldParameters : IFieldParameters;
  @Input('value') fieldValue : any;

  @Output() inputData = new EventEmitter<string>();


  constructor() {}

  sendDataToParent() {
    this.inputData.emit(this.fieldValue);
  }

  ngOnInit(){}

}

/*
@Input() fields: string[];
@Input() dataFormat: any;
@Input() isEditable: boolean = true;
@Input() fieldName: string;
@Input() data: any;

line: any;
lines: any;


//data=['chuj','cipa'];
constructor() { }

ngOnInit(): void {
  if (isDevMode()) console.log("One2manyFormComponent:ngOnInit");
  if (isDevMode()) console.log("One2manyFormComponent:ngOnInit:dataFormat");
  if (isDevMode()) console.log(this.dataFormat);
  
}
*/

