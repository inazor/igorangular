import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IWidgetParameters } from '../../interfaces/widgetParameters';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetFile',
  templateUrl: './widgetFile.component.html',
  styleUrls: ['./widgetFile.component.css']
})
export class WidgetFileComponent implements OnInit {

  @Input() wp : IWidgetParameters;
  @Input('p') fieldParameters : IFieldParameters;
  @Input('value') fieldValue : any;  
  @Output() inputData = new EventEmitter<string>();


  constructor() {}

  sendDataToParent() {
    this.inputData.emit(this.wp.FieldValue);
  }

  ngOnInit(){}
}

