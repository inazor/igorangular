import { Component, OnInit, Input } from '@angular/core';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetIdentifier',
  templateUrl: './widgetIdentifier.component.html',
  styleUrls: ['./widgetIdentifier.component.css']
})
export class WidgetIdentifierComponent implements OnInit {
  @Input('p') fieldParameters : IFieldParameters;
  @Input('value') fieldValue : any;

  isReadyToDisplayHtml: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
