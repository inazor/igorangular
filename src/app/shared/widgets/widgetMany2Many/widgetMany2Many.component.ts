import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IWidgetParameters } from '../../interfaces/widgetParameters';
import { MultiSelect, SelectItem } from 'primeng/primeng';
import { CRUDService } from '../../.services/crud.service';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetMany2Many',
  templateUrl: './widgetMany2Many.component.html',
  styleUrls: ['./widgetMany2Many.component.css']
})
export class WidgetMany2ManyComponent implements OnInit {
  @Input() wp: IWidgetParameters;
  @Input('p') fieldParameters : IFieldParameters;
  @Input('value') fieldValue : any;
  @Output() inputData = new EventEmitter<number[]>();

  @ViewChild('multiSelect', { static: false }) multiSelect: MultiSelect;

  recordNames: SelectItem[] = [];
  selectedData: any = null;
  notEditableData: any[] = [];

  constructor(private crudService: CRUDService) { }

  sendDataToParent() {
    this.inputData.emit(this.selectedData);
  }

  ngOnInit() {
    // if (this.wp.FieldDataFormat.Editable) {
    //   var recordNamesUrl = this.wp.ModelsDescription[this.wp.FieldDataFormat.RelatedModel].RecordNames;
    //   this.crudService.getRecordNames(recordNamesUrl).subscribe(response => {
    //     response.Data.forEach(element => {
    //       this.recordNames.push({ value: element[0], label: element[1] });
    //     })

    //     if (this.wp.FieldData != null && this.wp.FieldData != undefined) {
    //       this.wp.FieldData.forEach(element => {
    //         this.selectedData.push(element[0]);
    //       });
    //       this.sendDataToParent();
    //     }
    //     this.multiSelect.updateLabel();
    //   })
    // }
    // else {
    //   if (this.wp.FieldData != null && this.wp.FieldData != undefined) {
    //     this.wp.FieldData.forEach(element => {
    //       this.selectedData.push(element[1]);
    //       this.notEditableData.push(element[1]);
    //     });
    //     this.inputData.emit(this.notEditableData);

    //   }
    // }
  }
}


// @Input() recordNamesUrl: string;
// @Input() data: any[];
// @Input() isRequired: boolean;
// @Input() isEditable: boolean = true;
// @Output() inputData = new EventEmitter<number[]>();

// selectedData: number[] = [];
// notEditableData: any[] = [];
// recordNames: SelectItem[] = [];
// constructor(private crudService: CRUDService) { }
// @ViewChild('multiSelect', { static: false }) multi: MultiSelect;
// ngAfterViewInit(){
// }
// ngOnInit(): void {
//   // if (this.data != null && this.data != undefined) {
//   //   console.log("chuj");
//   //   this.data.forEach(element => {
//   //     this.selectedData.push(element[0]);
//   //   });
//   //   this.sendDataToParent();
//   // }
//   if(this.isEditable) {
//   this.crudService.getRecordNames(this.recordNamesUrl).subscribe(response => {
//     response.Data.forEach(element => {
//       this.recordNames.push({ value: element[0], label: element[1] });
//     })

//     if (this.data != null && this.data != undefined) {
//       this.data.forEach(element => {
//         this.selectedData.push(element[0]);
//       });
//       this.sendDataToParent();
//     }
//     this.multi.updateLabel();
//   }
//   )
// }
//   else {
//   if (this.data != null && this.data != undefined) {
//     this.data.forEach(element => {
//       this.selectedData.push(element[1]);
//       this.notEditableData.push(element[1]);
//     });
//     this.inputData.emit(this.notEditableData);
//   }
// }


// }
// sendDataToParent() {
//   this.inputData.emit(this.selectedData);
// }
