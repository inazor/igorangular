import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';
import { IDataFormat } from '../../interfaces/dataFormat';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetString',
  templateUrl: './widgetString.component.html',
  styleUrls: ['./widgetString.component.css']
})
export class WidgetStringComponent implements OnInit {

  
    @Input('p') fieldParameters : IFieldParameters;
    @Input('value') fieldValue : any;
    @Input('editable') pEditable : boolean = null;

    @Output() inputData = new EventEmitter<string>();
  
    isReadyToDisplayHtml: boolean = false;
    
    isEditable : boolean = null;
    isRequired : boolean = null;

    constructor() {}
  
    sendDataToParent() {
      this.inputData.emit(this.fieldValue);
    }

    ngOnInit(){

      this.setEditable();
      this.setRequired();
      this.isReadyToDisplayHtml = true;
    }

    setEditable(){
      this.isEditable = 
      this.pEditable != null 
      ? this.pEditable
      : this.fieldParameters.FieldDataFormat.Editable;
    }

    setRequired(){
      this.isRequired = this.fieldParameters.FieldDataFormat.Required;
    }
  }

