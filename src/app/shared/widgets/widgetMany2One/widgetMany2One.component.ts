import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IWidgetParameters } from '../../interfaces/widgetParameters';
import { CRUDService } from '../../.services/crud.service';
import { SelectItem } from 'primeng/api';
import { IFieldParameters } from '../../interfaces/fieldParameters';

@Component({
  selector: 'widgetMany2One',
  templateUrl: './widgetMany2One.component.html',
  styleUrls: ['./widgetMany2One.component.css']
})
export class WidgetMany2OneComponent implements OnInit {

  @Input() wp: IWidgetParameters;
  @Input('p') fieldParameters : IFieldParameters;
  @Input('value') fieldValue : any;  
  @Output() inputData = new EventEmitter<string>();

  recordNames: SelectItem[] = [];
  selectedData: any = null;

  constructor(private crudService: CRUDService) { }

  sendDataToParent() {
    this.inputData.emit(this.wp.FieldValue);
  }

  ngOnInit() {
    // if (this.wp.FieldDataFormat.Editable) {
    //   var recordNamesUrl = this.wp.ModelsDescription[this.wp.FieldDataFormat.RelatedModel].RecordNames;
    //   this.crudService.getRecordNames(recordNamesUrl).subscribe(response => {
    //     response.Data.forEach(element => {
    //       this.recordNames.push({ value: element[0], label: element[1] });
    //     })

    //     if (this.wp.FieldData != null && this.wp.FieldData != undefined) {
    //       this.selectedData = this.wp.FieldData[0];
    //       this.sendDataToParent();
    //     }
    //   })
    // }
    // else {
    //   if (this.wp.FieldData != null && this.wp.FieldData != undefined) {
    //     this.selectedData = this.wp.FieldData[1];
    //     this.inputData.emit(this.wp.FieldData[0]);
    //   }
    // }
  }
}