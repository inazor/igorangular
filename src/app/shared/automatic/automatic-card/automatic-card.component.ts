import { Component, OnInit, ContentChildren, ViewChild, QueryList, Input, isDevMode, Output } from '@angular/core';
import { CRUDService } from '../../.services/crud.service';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import{ EventEmitter} from '@angular/core';
import { isNumber } from 'util';

@Component({
	selector: 'automatic-card',
	templateUrl: './automatic-card.component.html',
	styleUrls: ['./automatic-card.component.css']
})
export class AutomaticCardComponent implements OnInit {
	@Input() modelName: string;
	@Input() listRoute;
	@Output() onLoaded = new EventEmitter();
	@Input() actions: any = null;
	actionKeys: string[] = [];
	itemId: number = null;
	dataFormat: any[] = [];

	allModelsDescription: any;
	modelDataFormat: any;
	modelData: any;


	crudServicePrefix: string;
	data: any;

	permissions= JSON.parse(sessionStorage.getItem('permissions'));
	private modelDescription: any = {};
	@ContentChildren(NgModel) public models: QueryList<NgModel>;
	@ViewChild(NgForm, { static: false }) public form: NgForm;

	public ngAfterViewInit(): void {
		let ngContentModels = this.models.toArray();
		ngContentModels.forEach((model) => {
			this.form.addControl(model);
		});
	}


	ngAfterContentInit() {

	};

	constructor(private crudService: CRUDService, private router: Router) { }

	ngOnInit() {
		console.log(this.actions);
		this.actions = JSON.parse(this.actions);
		console.log(this.actions);
		this.actionKeys = this.actions !== null  ? Object.keys(this.actions) : [];
		if (isDevMode()) console.log("AutomaticCardHeaderComponent:ngOnInit");
		if (isDevMode()) console.log("AutomaticCardHeaderComponent:ngOnInit: modelName:" + this.modelName);


		if (this.modelName === "" || this.modelName == null)
			throw new Error("AutomaticCardHeaderComponent: Model parameter not specified");

		localStorage.setItem('AutomaticCard_modelName', this.modelName);
		//check if itemId was sent via local storage 
		//TODO: probabbly this should be done some other way, by sending messages between controls..
		this.itemId = + localStorage.getItem('automaticList_selectedItemId');
		if (this.itemId == null || this.itemId == undefined)
			throw new Error("Parameter Item ID not passed via local storage");

		if (isDevMode()) console.log("AutomaticCardHeaderComponent:constructor: itemId:" + this.itemId);
		localStorage.setItem('AutomaticCard_itemId', this.itemId + "");

		//get data from crudservice
		this.crudService.getModelDescription().subscribe((response: any) => {
			if (!response.hasOwnProperty("Data"))
				throw new Error("Model Description response does not have 'Data' key");

			if (!response.Data.hasOwnProperty("ModelsDescription"))
				throw new Error("Model Description response does not have 'ModelsDescription' key");
			this.allModelsDescription = response.Data.ModelsDescription;

			console.log(this.modelName)
			if (!response.Data.ModelsDescription.hasOwnProperty(this.modelName))
				throw new Error("Model Description response does not have definition for model " + this.modelName);
			this.modelDescription = this.allModelsDescription[this.modelName];
			localStorage.setItem('AutomaticCard_modelDescription', JSON.stringify(this.allModelsDescription));


			if (!this.modelDescription.hasOwnProperty("CrudServicePrefix"))
				throw new Error("ModelDescription for model '" + this.modelName + "' does not contain key 'CrudServicePrefix'");
			this.crudServicePrefix = this.modelDescription.CrudServicePrefix;

			if (isDevMode()) console.log("AAAAAAAA");
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit:allModelsDescription");
			if (isDevMode()) console.log(this.allModelsDescription);
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit:modelDescription");
			if (isDevMode()) console.log(this.modelDescription);
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit:crudServicePrefix " + this.crudServicePrefix);

			// localStorage.setItem('AutomaticCard_allModelsDescription', JSON.stringify(this.allModelsDescription));
			// localStorage.setItem('AutomaticCard_modelDescription', JSON.stringify(this.modelDescription));
			//Get data format
			this.crudService.getModel(this.crudServicePrefix).subscribe(response => {
				if (!response.hasOwnProperty("Data"))
					throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'Data' key");

				if (!response.Data.hasOwnProperty("DataFormat"))
					throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'DataFormat' key");
				console.log(response.Data.DataFormat);
				this.modelDataFormat = response.Data.DataFormat;

				// localStorage.setItem('AutomaticCard_modelDataFormat', JSON.stringify(this.modelDataFormat));

				if (isDevMode()) console.log("BBBBBBB");
				if (isDevMode()) console.log("FieldFormComponent:ngOnInit:modelDataFormat");
				if (isDevMode()) console.log(this.modelDataFormat);
				localStorage.setItem('AutomaticCard_modelDataFormat', JSON.stringify(this.modelDataFormat));
				console.log(this.modelDataFormat);
			});
			if (localStorage.getItem('automaticList_selectedItemId')) {

				//get data
				this.crudService.getOne(this.crudServicePrefix, this.itemId).subscribe(response => {
					this.modelData = response.Data;
					//   this.data = this.modelData[this.fieldName];	
					localStorage.setItem('AutomaticCard_modelData', JSON.stringify(this.modelData));

				});
			}
			this.onLoaded.emit(null);
		});
	}

	onSubmit(f) {
		if(isNumber(f.form.value.Id))
		this.crudService.editOne(this.crudServicePrefix,f.form.value).subscribe();
		else
		this.crudService.addOne(this.crudServicePrefix,f.form.value).subscribe();
	}

	cancel() {
		this.router.navigate([this.listRoute]);
	}
	doAction(actionData: any){
		this.crudService.postData(actionData.ApiCall,null).subscribe(
			suc => {
           this.router.navigate(['/' + actionData.RouteOnSuccess])
        },
        err => {
            this.router.navigate(['/' + actionData.RouteOnError])
        })
	}
}
