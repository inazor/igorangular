import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticCardComponent } from './automatic-card.component';

describe('AutomaticCardComponent', () => {
  let component: AutomaticCardComponent;
  let fixture: ComponentFixture<AutomaticCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomaticCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
