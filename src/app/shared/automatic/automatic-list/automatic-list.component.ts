import { Component, OnInit, Input, isDevMode } from '@angular/core';
import { CRUDService } from '../../.services/crud.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
@Component({
  selector: 'automatic-list',
  templateUrl: './automatic-list.component.html',
  styleUrls: ['./automatic-list.component.css']
})
export class AutomaticListComponent implements OnInit {

  @Input() model: string = "";
  @Input() title: string = "";
  @Input() fields: string[] = [];
  @Input() itemCardRoute: string = "";

  dataFormat: any;
  data: any;
  modelDescription: any;
  allModelsDescription;
  crudServicePrefix;



  constructor(private crudService: CRUDService, private router: Router, private confirmationService: ConfirmationService) { }

  ngOnInit() {

    //Validation
    if (this.model == "" || this.model === undefined)
      throw new Error("AutomaticListComponent:ngOnChanges: parameter 'model' not specified");
    this.crudService.getModelDescription().subscribe((response: any) => {
      if (!response.hasOwnProperty("Data"))
        throw new Error("Model Description response does not have 'Data' key");
      if (!response.Data.hasOwnProperty("ModelsDescription"))
        throw new Error("Model Description response does not have 'ModelsDescription' key");
      this.allModelsDescription = response.Data.ModelsDescription;

      if (!response.Data.ModelsDescription.hasOwnProperty(this.model))
        throw new Error("Model Description response does not have definition for model " + this.model);
      this.modelDescription = this.allModelsDescription[this.model];

      if (!this.modelDescription.hasOwnProperty("CrudServicePrefix"))
        throw new Error("ModelDescription for model '" + this.model + "' does not contain key 'CrudServicePrefix'");
      this.crudServicePrefix = this.modelDescription.CrudServicePrefix;

      //Get data format
      this.crudService.getModel(this.crudServicePrefix).subscribe(response => {
        console.log(response);
        if (!response.hasOwnProperty("Data"))
          throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'Data' key");

        if (!response.Data.hasOwnProperty("DataFormat"))
          throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'DataFormat' key");
        this.dataFormat = response.Data.DataFormat;
        // if (isDevMode()) console.log("BBBBBBB");
        // if (isDevMode()) console.log(this.dataFormat);
      });
      // if (isDevMode()) console.log("AAAAAAAA");
      // if (isDevMode()) console.log(this.allModelsDescription);
      // if (isDevMode()) console.log(this.modelDescription);
      // if (isDevMode()) console.log("crudServicePrefix "  + this.crudServicePrefix);
      //get data
      this.crudService.getAll(this.crudServicePrefix).subscribe(response => {
        if (!response.hasOwnProperty("Data"))
          throw new Error("Data response for model '" + this.crudServicePrefix + "'does not have 'Data' key");

        this.data = response.Data;

        // if (isDevMode()) console.log("CCCCCCC");
        // if (isDevMode()) console.log("crudServicePrefix "  + this.crudServicePrefix);
        // if (isDevMode()) console.log(this.data);
      });
    });



  }

  add() {
    localStorage.removeItem('automaticList_selectedItemId');
    this.resetLocalStorage();
    this.router.navigate([this.itemCardRoute]);
  }

  openDetails(rowData: any) {
    if (this.itemCardRoute == "")
      throw new Error("Item card route not specified in the form declaration");
    this.resetLocalStorage();
    localStorage.setItem('automaticList_selectedItemId', rowData.Id);
    this.router.navigate([this.itemCardRoute + "/" + rowData.Id]);

  }
  resetLocalStorage() {
    localStorage.removeItem('AutomaticCard_modelName');
    localStorage.removeItem('AutomaticCard_modelDataFormat');
    localStorage.removeItem('AutomaticCard_modelData');
    localStorage.removeItem('AutomaticCard_itemId');
    localStorage.removeItem('AutomaticCard_modelDescription');
  }
  confirm(rowData) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this row?',
      accept: () => {
        this.delete(rowData);
      }
    });
  }
  delete(rowData) {
    this.crudService.deleteOne(this.model, rowData.Id);
  }
}
