import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListComponent } from './controls/list/list.component';
import { CardComponent } from './controls/card/card.component';
import { FieldComponent } from './controls/field/field.component';

import { WidgetLabelComponent } from './widgets/widgetLabel/widgetLabel.component';
import { WidgetIdentifierComponent } from './widgets/widgetIdentifier/widgetIdentifier.component';
import { WidgetStringComponent } from './widgets/widgetString/widgetString.component';
import { WidgetMany2OneComponent } from './widgets/widgetMany2One/widgetMany2One.component';
import { WidgetFileComponent } from './widgets/widgetFile/widgetFile.component';
import { WidgetMany2ManyComponent } from './widgets/widgetMany2Many/widgetMany2Many.component';
import { WidgetOne2ManyComponent } from './widgets/widgetOne2Many/widgetOne2Many.component';
import { WidgetIntegerComponent } from './widgets/widgetInteger/widgetInteger.component';


 

import { Input_requiredDirective } from './directives/input_required.directive';


import { AutomaticCardComponent } from './automatic/automatic-card/automatic-card.component';
import { AutomaticListComponent } from './automatic/automatic-list/automatic-list.component';

import { FieldListComponent } from './shared-view-helpers/list/field-list/field-list.component';
import { FieldFormComponent } from './shared-view-helpers/form/field-form/field-form.component';

import { StringListComponent } from './shared-view-helpers/list/string-list/string-list.component';
import { StringFormComponent } from './shared-view-helpers/form/string-form/string-form.component';

import { DateListComponent } from './shared-view-helpers/list/date-list/date-list.component';
import { DateFormComponent } from './shared-view-helpers/form/date-form/date-form.component';

import { IdentifierListComponent } from './shared-view-helpers/list/identifier-list/identifier-list.component';
import { IdentifierFormComponent } from './shared-view-helpers/form/identifier-form/identifier-form.component';

import { Many2oneformComponent } from './shared-view-helpers/form/many2oneform/many2oneform.component';

import { AppErrorComponent } from '../root/app.error.component';

//primeng modules
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AccordionModule } from 'primeng/accordion';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { PanelModule } from 'primeng/panel';
import { EditorModule } from 'primeng/editor';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { SpinnerModule } from 'primeng/spinner';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TreeTableModule } from 'primeng/treetable';
import { PickListModule } from 'primeng/picklist';
import { FileUploadModule } from 'primeng/fileupload';

import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DropdownModule, InputTextModule } from 'primeng/primeng';
import { AppRoutingModule } from '../root/app-routing.module';
import { LabelFormComponent } from './shared-view-helpers/form/label-form/label-form.component';
import { Many2manyformComponent } from './shared-view-helpers/form/many2manyform/many2manyform.component';
import { One2manyFormComponent } from './shared-view-helpers/form/one2many/one2many.component';
import { StringselectionFormComponent } from './shared-view-helpers/form/stringselection-form/stringselection-form.component';
import { BooleanFormComponent } from './shared-view-helpers/form/boolean-form/boolean-form.component';
import { FileFormComponent } from './shared-view-helpers/form/file-form/file-form.component';
import { BooleanListComponent } from './shared-view-helpers/list/boolean-list/boolean-list.component';



@NgModule({
	exports: [
		AccordionModule,
		ToastModule,
		MultiSelectModule,
		TableModule,
		CheckboxModule,
		FormsModule,
		AutoCompleteModule,
		DropdownModule,
		PanelModule,
		AppRoutingModule,
		ConfirmDialogModule,
		MessageModule,
		CommonModule,
		InputTextModule,

		ListComponent,
		CardComponent,
		FieldComponent,
		WidgetStringComponent,


		WidgetIntegerComponent,
		WidgetLabelComponent,
		WidgetIdentifierComponent,
		WidgetMany2OneComponent,
		WidgetFileComponent,
		WidgetMany2ManyComponent,
		WidgetOne2ManyComponent,

		Input_requiredDirective,

		AutomaticListComponent,
		AutomaticCardComponent,
		FieldFormComponent,
		RadioButtonModule,
		SelectButtonModule,
		EditorModule,
		InputSwitchModule,
		ToggleButtonModule,
		SpinnerModule,
		SplitButtonModule,
		TreeTableModule,
		PickListModule,
		FileUploadModule
	],
	declarations: [
		ListComponent,
		CardComponent,
		FieldComponent,

		WidgetStringComponent,
		WidgetIntegerComponent,
		WidgetLabelComponent,
		WidgetIdentifierComponent,
		WidgetMany2OneComponent,
		WidgetFileComponent,
		WidgetMany2ManyComponent,
		WidgetOne2ManyComponent,

		Input_requiredDirective,

		AutomaticListComponent,
		AutomaticCardComponent,
		FieldListComponent,
		FieldFormComponent,
		StringListComponent,
		StringFormComponent,
		DateListComponent,
		DateFormComponent,
		IdentifierListComponent,
		IdentifierFormComponent,
		Many2oneformComponent,
		LabelFormComponent,
		Many2manyformComponent,
		One2manyFormComponent,

		AppErrorComponent,
		StringselectionFormComponent,

		BooleanFormComponent,

		FileFormComponent,

		BooleanListComponent,

	],
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		//primeng modules
		AccordionModule,
		AutoCompleteModule,
		CalendarModule,
		CardModule,
		CheckboxModule,
		ConfirmDialogModule,
		DialogModule,
		DropdownModule,
		FullCalendarModule,
		MessagesModule,
		MessageModule,
		MultiSelectModule,
		SelectButtonModule,
		TableModule,
		ToastModule,
		PanelModule,
		DropdownModule,
		AppRoutingModule,
		InputTextModule,
		RadioButtonModule,
		EditorModule,
		InputSwitchModule,
		ToggleButtonModule,
		SpinnerModule,
		SplitButtonModule,
		TreeTableModule,
		PickListModule,
		FileUploadModule

	]
})
export class SharedModule { }
