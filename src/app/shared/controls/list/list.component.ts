import { Component, OnInit, Input, EventEmitter, Output, ContentChild, ContentChildren, QueryList,AfterContentInit
} from '@angular/core';
import { CRUDService } from '../../.services/crud.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FieldParametersService } from '../../.services/fieldParameters.service';
import { IFieldParameters } from '../../interfaces/fieldParameters';
import { isNumber } from 'util';
import { EViewLayout } from '../../enums/viewLayout.enum';
import { IModelDescription } from '../../interfaces/modelDescription';
import { resolveSanitizationFn } from '@angular/compiler/src/render3/view/template';
import { EFieldDisplayMode } from '../../enums/fieldDisplayMode.enum';
import { IDataFormat } from '../../interfaces/dataFormat';
import { FieldComponent } from '../field/field.component';
import { IModelDetails } from '../../interfaces/modelDetails';
import { IFieldDataFormat } from '../../interfaces/fieldDataFormat';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input('model') modelName: string;
  @Input() listRoute;
  @Output() onLoaded = new EventEmitter();
  @Input() actions: any = null;

  @ContentChildren(FieldComponent) contentChildren:QueryList<FieldComponent>;


/*
  modelsDescription  - URLs for all models, 

  modelDetails - Name, NamePlural, Dataformat for one model

  dataFormat - all parameters of a field  - New version: FieldDataFormat

  ModelDataFormat :  {[fieldName: string]: IFieldDataFormat}


*/


  actionsJson = JSON.parse("{}");
  actionKeys = [];
  crudServicePrefix: string;
  permissions:any = JSON.parse(sessionStorage.getItem("permissions"));

  modelsDescription: {[modelName: string]: IModelDescription};
  modelDataFormat: {[fieldName: string]: IFieldDataFormat} = null;
  modelData: [ { [fieldName: string]: any } ] //list of dictionaries, where key denotes fieldName
  
  
  //helpers for templating engine
  recordIdsObjectArray: any = [];
  fieldNamesObjectArray: any = [];
  dataTypesDict: { [fieldName: string]: string } = {};
  modelDataDict: { [Id: number]: { [fieldName: string]: any } } = {};

  isInitialized: boolean = false;
  isContentInitialized: boolean = false;
  isReadyToDisplayHtml: boolean = false;

	EFieldDisplayMode = EFieldDisplayMode;   	//fix to allow enums to be seen in html

  itemCardRoute: any;

  fieldsParameters: {[fieldName: string] : IFieldParameters} = {};
  shownFieldsNames: string[] = [];


  constructor(private crudService: CRUDService, private router: Router, private route: ActivatedRoute, private fieldParametersService: FieldParametersService) { }

  ngAfterViewInit(): void {

    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    var a=1;
  }

  ngAfterContentInit(){
    this.contentChildren.forEach(element => {
      this.shownFieldsNames.push(element.fieldName);
    });

    this.isContentInitialized = true;
    this.getDescriptionFormatData();
  }

  ngOnInit() {
    this.parseActionsJson();
    
    this.isInitialized = true;
    this.getDescriptionFormatData();
  }

  onSubmit(f) {
		if(isNumber(f.form.value.Id) && f.form.value.Id > 0 )
		  this.crudService.editOne(this.crudServicePrefix,f.form.value).subscribe();
		else
		  this.crudService.addOne(this.crudServicePrefix,f.form.value).subscribe();
  }
  
	doAction(actionData: any){
		this.crudService.postData(actionData.ApiCall,null).subscribe(
			suc => {
           this.router.navigate(['/' + actionData.RouteOnSuccess])
        },
        err => {
            this.router.navigate(['/' + actionData.RouteOnError])
        })
  }  
  
  cancel() {
		this.router.navigate([this.listRoute]);
  }
  addNewRow() {
    localStorage.removeItem('automaticList_selectedItemId');
    // this.resetLocalStorage();
    this.router.navigate([this.itemCardRoute]);
  }

  openCard(id: number) {

    if (id == null || id == 0)
      throw new Error("Record ID specified in the OpenCard function");

    this.router.navigate(["base-role-card/" + id]);
  
  }

  parseActionsJson(){
    // try {
    //   this.actionsJson = JSON.parse(this.actions);

    //   if (typeof this.actions === 'object' && this.actions != null) {
    //     this.actionKeys = Object.keys(this.actionsJson);
    //   }
    // }
    // catch(e) {
    //   console.log("Failed to parse json!");
    //   throw new Error(e);
    // }
  }


  //#region set helper data structures to pass on to widgets
    setFieldsParameters (){
        this.shownFieldsNames.forEach(fieldName => {
          this.fieldsParameters[fieldName] = {
            FieldName : fieldName,
            FieldDataFormat : this.modelDataFormat[fieldName],
            FieldData: null,
        
            RecordId: null,
        
            ModelName : this.modelName,
            ModelDataFormat: this.modelDataFormat,
            ModelData: this.modelData,
        
            ModelsDescription : this.modelsDescription,
        
            ViewLayout: EViewLayout.List,
        }
        });
    }
    
    setDataTypesDict (){
      this.shownFieldsNames.forEach(fieldName => {
        this.dataTypesDict[fieldName] = this.modelDataFormat[fieldName].DataType;
      });
    }

    setModelDataDict(){
      this.modelData.forEach(element => {
        this.modelDataDict[element.Id] = element;
      }); 
    }

    setFieldNamesObjectArray(){
      this.shownFieldsNames.forEach(fieldName => {
        this.fieldNamesObjectArray.push({FieldName : fieldName})
      });
    }
  
    setRecordIdsObjectArray(){
      this.modelData.forEach(element => {
        this.recordIdsObjectArray.push({Id : element.Id})
      }); 
    }

  //#endregion
  
  getDescriptionFormatData(){
    //both initialization triggers must trip
    if ( this.isContentInitialized == false || this.isInitialized == false ){
      return;
    }
      
    if (this.modelName == null ){
      throw new Error("getModelDescriptionAndFieldsDataFormat modelName not passed");
    }

    if (this.shownFieldsNames.length == 0){
      throw new Error("getModelDescriptionAndFieldsDataFormat Bo fields to show");
    }

    //get desctription of all models (modelName & crudServicePrefix)
    this.crudService.getModelsDescription().subscribe((response: any) => {
      if (!response.hasOwnProperty("Data")) 
        throw new Error("Data format response getModelsDescription does not have 'Data' key");
      if (!response.Data.hasOwnProperty("ModelsDescription")) 
        throw new Error("Data format response for getModelsDescription does not have 'ModelsDescription' key");
      
      var data = response.Data.ModelsDescription as {[modelName: string]: IModelDescription};
      
      this.modelsDescription = data;

      if ( !this.modelsDescription.hasOwnProperty(this.modelName) ) 
        throw new Error("Model Description response does not have definition for model " + this.modelName);

      //set description of our model
      var modelDescription : IModelDescription = this.modelsDescription[this.modelName];
      this.crudServicePrefix = modelDescription.CrudServicePrefix;

      //get data format for our model
      this.crudService.getModelDetails(this.crudServicePrefix).subscribe(response => {
        if (!response.hasOwnProperty("Data")) 
          throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'Data' key");

        var data = response["Data"] as IModelDetails;

        this.modelDataFormat = data.DataFormat;
        var modelNamePlural : string = data.NamePlural;

        //get data 
        this.crudService.getAll(this.crudServicePrefix).subscribe(response => {
          if (!response.hasOwnProperty("Data")) 
            throw new Error("crudService.getAll: wrong data response for model " + this.crudServicePrefix);

          this.modelData = response.Data as [{[fieldName: string]: any}];    

          //set helper data structures to pass on to widgets
          this.setModelDataDict();
          this.setFieldsParameters();
          this.setDataTypesDict();
          this.setRecordIdsObjectArray();
          this.setFieldNamesObjectArray();
          
          console.log(this.modelDataDict);
          console.log(this.fieldsParameters);
          console.log(this.dataTypesDict);
          console.log(this.recordIdsObjectArray);
          console.log(this.fieldNamesObjectArray);

          this.isReadyToDisplayHtml = true;
        })
      })
    })    
  }
}

