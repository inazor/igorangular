import { Component, OnInit, ContentChildren, ViewChild, QueryList, Input, isDevMode, Output, AfterViewInit, EmbeddedViewRef } from '@angular/core';
import { CRUDService } from '../../.services/crud.service';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { isNumber } from 'util';
import { hostViewClassName, NullTemplateVisitor } from '@angular/compiler';
import { throttleTime } from 'rxjs/operators';
import { FieldParametersService } from '../../.services/fieldParameters.service';
import { IFieldParameters } from '../../interfaces/fieldParameters';
import { EViewLayout } from '../../enums/viewLayout.enum';
import { FieldComponent } from '../field/field.component';
import { IModelDescription } from '../../interfaces/modelDescription';
import { IFieldDataFormat } from '../../interfaces/fieldDataFormat';
import { IModelDetails } from '../../interfaces/modelDetails';
import { IRecordParameters } from '../../interfaces/recordParameters';
import { RecordParametersService } from '../../.services/recordParameters.service';


@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  
  @Input('modelName') modelName: string;
  @Input() listRoute;

  @Output() onLoaded = new EventEmitter();
  @Input() actions: any = null;

  @ContentChildren(FieldComponent) contentChildren: QueryList<FieldComponent>;

  actionsJson = JSON.parse("{}");
  actionKeys = [];

  isInitialized: boolean = false;
  isContentInitialized: boolean = false;
  isReadyToDisplayHtml: boolean = false;

  crudServicePrefix: string;
  permissions: any = JSON.parse(sessionStorage.getItem("permissions"));
  p: IRecordParameters = {

    RecordId: null,
    RecordData: {},    

    ModelName : null,
    ModelDataFormat: {},

    ModelsDescription : null,

    ViewLayout: EViewLayout.Card,    

  };

  shownFieldsNames: string[] = [];
  fieldsParameters: {[fieldName: string] : IFieldParameters} = {};

  modelsDescription: {[modelName: string]: IModelDescription}; //recordNames URL, crudserviceprefix for all models
  modelDataFormat: {[fieldName: string]: IFieldDataFormat} = null; //data format for all fields in model
  
  recordId : number = null;
  recordData: { [fieldName: string]: any } //ONE DICTIONARY, fieldname:value
  
  
  constructor(
    private crudService: CRUDService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private recordParametersService: RecordParametersService
    ) { }

  ngAfterContentInit() {
    console.log("CardComponent:ngAfterContentInit. modelName:" + this.modelName);
    //get data from services  (model description, etc)
    this.isContentInitialized = true;
    this.getDescriptionFormatData();
  }

  ngAfterViewInit(): void {
    console.log("CardComponent:ngAfterContentInit. modelName:" + this.modelName);
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    var a = 1;
  }

  ngOnInit() {
    console.log("CardComponent:ngAfterContentInit. modelName:" + this.modelName);
    //this.parseActionsJson();

    this.isInitialized = true;
    this.getDescriptionFormatData();
  }

  onSubmit(f) {
    if (isNumber(f.form.value.Id) && f.form.value.Id > 0)
      this.crudService.editOne(this.crudServicePrefix, f.form.value).subscribe();
    else
      this.crudService.addOne(this.crudServicePrefix, f.form.value).subscribe();
  }

  doAction(actionData: any) {
    this.crudService.postData(actionData.ApiCall, null).subscribe(
      suc => {
        this.router.navigate(['/' + actionData.RouteOnSuccess])
      },
      err => {
        this.router.navigate(['/' + actionData.RouteOnError])
      })
  }

  cancel() {
    this.router.navigate([this.listRoute]);
  }

  parseActionsJson() {
    try {
      this.actionsJson = JSON.parse(this.actions);

      if (typeof this.actions === 'object' && this.actions != null) {
        this.actionKeys = Object.keys(this.actionsJson);
      }
    }
    catch(e) {
      console.log("Failed to parse json!");
      throw new Error(e);
    }
  }

  //get all data from services (subscribe)
  getDescriptionFormatData() {
    //both initialization triggers must trip
    if (this.isContentInitialized == false || this.isInitialized == false) {
      return;
    }
    console.log("CardComponent:getDescriptionFormatData. modelName:" + this.modelName);
    if (this.modelName == null) {
      throw new Error("getDescriptionFormatData: modelName not passed");
    }

    this.p.ModelName = this.modelName;

    //get desctription of all models (modelName & crudServicePrefix)
    this.crudService.getModelsDescription().subscribe((response: any) => {
      if (!response.hasOwnProperty("Data"))
        throw new Error("Data format response getModelsDescription does not have 'Data' key");
      if (!response.Data.hasOwnProperty("ModelsDescription"))
        throw new Error("Data format response for getModelsDescription does not have 'ModelsDescription' key");

      if (!response.Data.ModelsDescription.hasOwnProperty(this.modelName))
        throw new Error("Model Description response does not have definition for model " + this.modelName);

      var data = response.Data.ModelsDescription as { [modelName: string]: IModelDescription };
      
      this.p.ModelsDescription = data;

      //set description of our model
      var modelDescription: IModelDescription = data[this.modelName];
      this.crudServicePrefix = modelDescription.CrudServicePrefix;

      //get data format for our model
      this.crudService.getModelDetails(this.crudServicePrefix).subscribe(response => {
        if (!response.hasOwnProperty("Data"))
          throw new Error("Data format response for model '" + this.crudServicePrefix + "'does not have 'Data' key");

        var data = response["Data"] as IModelDetails;

        this.p.ModelDataFormat = data.DataFormat;
        
        //get item id
        this.route.params.subscribe(params => {
          if (params.hasOwnProperty('id') && params['id'] != null && +params['id'] > 0) {
            this.p.RecordId = +params['id'];

            //get data
            this.crudService.getOne(this.crudServicePrefix, this.p.RecordId).subscribe(response => {
              if (!response.hasOwnProperty("Data"))
                throw new Error("crudService.getOne: wrong data response for model " + this.crudServicePrefix);

              this.p.RecordData = response.Data as { [fieldName: string]: any }
              
              this.onLoaded.emit(null);

              //emit p to children
              this.recordParametersService.broadcast(this.p);

              this.isReadyToDisplayHtml = true;
            })
          }
        })
      })
    })
  }

}
