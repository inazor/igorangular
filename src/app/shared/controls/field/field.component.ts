import { Component, OnInit, Input, forwardRef, isDevMode, AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator } from '@angular/forms';
import { CRUDService } from 'src/app/shared/.services/crud.service';
import { FieldParametersService } from '../../.services/fieldParameters.service';
import { IFieldParameters } from '../../interfaces/fieldParameters';
import { IWidgetParameters } from '../../interfaces/widgetParameters';
import { EViewLayout } from '../../enums/viewLayout.enum';
import { EFieldDisplayMode } from '../../enums/fieldDisplayMode.enum';
import { RecordParametersService } from '../../.services/recordParameters.service';
import { IRecordParameters } from '../../interfaces/recordParameters';
import { IFieldDataFormat } from '../../interfaces/fieldDataFormat';



@Component({
	selector: 'field',
	templateUrl: './field.component.html',
	styleUrls: ['./field.component.css']
})
export class FieldComponent implements OnInit {
	@Input('name') fieldName: string;

	@Input() labelWidth: number = 4;
	@Input() displayMode: EFieldDisplayMode = null;
	
	recordParameters: IRecordParameters;
	fieldParameters: IFieldParameters;


	//fix to allow enums to be seen in html
	EViewLayout = EViewLayout; 
	EFieldDisplayMode = EFieldDisplayMode;

	isInitialized = false;

	propagateChange = (_: any) => { };

	registerOnValidatorChange?(fn: () => void): void {
	}

	writeValue(str: string): void {
		if (this.fieldParameters.FieldData != undefined && this.fieldParameters.FieldData != null)
			this.sendDataToForm(this.fieldParameters.FieldData);
	}
	registerOnChange(fn: any): void {
		this.propagateChange = fn;
	}
	registerOnTouched(fn: any): void {
	}
	setDisabledState?(isDisabled: boolean): void {
		throw new Error("Method not implemented.");
	}
	/*      
	  Description:
	  Sends data to form in parent component
	  Called from:
	   src\app\FormComponents\main-form\main-form.component.html
	   in
	   ngOnInit()
  
	  Input: 
	  Response object
	  
	  Output:
	  None
		  */
	sendDataToForm(childData) {
		this.fieldParameters.FieldData = childData;
		this.propagateChange(childData);
	}

	constructor(private crudService: CRUDService, private recordParametersService: RecordParametersService) { }

	validate() {
		if (this.fieldParameters.FieldDataFormat != undefined) {
			var data = this.fieldParameters.FieldData;
			if (
				(
					data != null
					&& data != undefined
					&& data != ""
				)
				|| !this.fieldParameters.FieldDataFormat.Required
			) {
				return null;
			} else {
				return { 'valid': false };
			}
		}
	}

	ngOnInit() {
		console.log("FieldComponent:ngOnInit. fieldName:" + this.fieldName);

		//Validation
		if (this.fieldName === undefined || this.fieldName === null || this.fieldName.length === 0) {
			throw new Error("FieldFormComponent: Required parameter 'name' not given");
		}

		//wait for recordparameters from card or list
		this.recordParametersService.viewParametersLoaded$.subscribe((response: IRecordParameters) => {

			this.recordParameters = response;
			
			this.setFieldParameters();

			this.setDisplayMode();
			this.sendDataToForm(this.fieldParameters.FieldData);
			console.log("FieldComponent:recordParametersService fieldName:" + this.fieldName);
			console.log(this.fieldParameters);
			this.isInitialized = true;
		});		

	}

	ngAfterContentInit(): void {
		console.log("FieldComponent:ngAfterContentInit fieldName:" + this.fieldName);
		//Called after ngOnInit when the component's or directive's content has been initialized.
		//Add 'implements AfterContentInit' to the class.
		var a = 1;
	}

	setFieldParameters(){

		if ( this.fieldName == null || this.fieldName.length == 0)
			throw new Error("FieldComponent:setFieldParameters FieldName not defined");

		this.validateIRecordParameters(this.recordParameters)

		this.fieldParameters = {
			FieldName : this.fieldName,
			FieldDataFormat : this.recordParameters.ModelDataFormat[this.fieldName],
			FieldData: this.recordParameters.RecordData[this.fieldName],
		
			RecordId: this.recordParameters.RecordId,
		
			ModelName : this.recordParameters.ModelName,
			ModelDataFormat: null,
			ModelData:  null,
		
			ModelsDescription : this.recordParameters.ModelsDescription,
		
			ViewLayout: this.recordParameters.ViewLayout,
		}

	}	

	setDisplayMode(){
		//if it comes from a Card, display normally
		//in all other instances wait for displayMode to be set as  @Input parameter
		
		if (this.displayMode === null && this.fieldParameters != null){
			var editable = this.fieldParameters.FieldDataFormat.Editable;
			var mode = this.fieldParameters.ViewLayout;
			
			if (mode == EViewLayout.Card)
				this.displayMode = EFieldDisplayMode.Card
			else 
				this.displayMode = EFieldDisplayMode.ListDataNonEditable;
		}
	}

	//TODO: move to separate library
    validateIRecordParameters(p: IRecordParameters){

		if (p===null)
		  throw new Error("RecordParametersService: empty parameters received ");
	
		if ( p.RecordId === null || p.RecordId == 0 )
		  throw new Error("RecordParametersService: RecordId not sent");
	
		if ( p.RecordData === null)
		  throw new Error("RecordParametersService: RecordData not sent");  
		  
		if ( p.ModelName == null || p.ModelName.length == 0 ) {
			throw new Error("RecordParametersService: ModelName not sent");
		  }        
	
		if (p.ModelDataFormat == null) {
		  throw new Error("RecordParametersService: ModelDataFormat not sent");
		}
	
		if ( p.ModelsDescription == null ) {
		  throw new Error("RecordParametersService: ModelsDescription not sent");
		}
	
	  }	
}
