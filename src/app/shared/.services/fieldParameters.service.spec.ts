/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FieldParametersService } from './fieldParameters.service';

describe('Service: FieldParameters', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FieldParametersService]
    });
  });

  it('should ...', inject([FieldParametersService], (service: FieldParametersService) => {
    expect(service).toBeTruthy();
  }));
});
