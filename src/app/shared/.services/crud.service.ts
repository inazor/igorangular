import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IModelDescription } from '../interfaces/modelDescription';

@Injectable({
	providedIn: 'root'
})
export class CRUDService {

	private httpOptions = {
		headers: new HttpHeaders({
			'Authorization': "Bearer " + localStorage.getItem('token'),
			'responseType': 'json'
		})
	};
	private baseUrl = localStorage.getItem('BaseURL');
	constructor(private http: HttpClient) { }

	getModel(endUrl: string) {
		//TODO: replace with getModelDetails
		var url = this.baseUrl + endUrl;
		if (isDevMode()) console.log("CRUDService:getModel:url: " + url);
		return this.http.get<any>(url + '/model');
	}

	getModelDetails(endUrl: string) {
		var url = this.baseUrl + endUrl;
		if (isDevMode()) console.log("CRUDService:getModelDetails:url: " + url);
		return this.http.get<any>(url + '/model');
	}

	getModelsDescription() {
		var url = this.baseUrl + 'model/description';

		if (isDevMode()) console.log("CRUDService:getModelsDescription url:" + url);

		return this.http.get<any>(url, this.httpOptions);

		// if (result.hasOwnProperty("Data") && result["Data"].hasOwnProperty("ModelsDescription")){
		// 	return result["Data"]["ModelsDescription"];
			
		// }
		// else
		// 	{
		// 		console.log( "Wrong response format getModelsDescription" );
		// 		console.log( result );
		// 		throw new Error( "Wrong response format getModelsDescription " + result );
		// 	};
	}
	getModelDescription() {
		//TODO: replace with getModelsDescription
		var url = this.baseUrl + 'model/description';

		if (isDevMode()) console.log("CRUDService:getModelsDescription url:" + url);

		var result = this.http.get<any>(url, this.httpOptions);

		return result;


	}	
	getRecordNames(endUrl: string) {
		var url = this.baseUrl + endUrl.substring(1, endUrl.length);
		// if (isDevMode()) console.log("CRUDService:getRecordNames url:" + url);
		// return this.http.get<any>(url,this.httpOptions);

		if (isDevMode()) console.log("CRUDService:getRecordNames url:" + url);
				var result = this.http.get<any>(url, this.httpOptions);
			return result;
	
	}
	getAll(endUrl: string) {
		var url = this.baseUrl + endUrl;
		// if (isDevMode()) console.log("CRUDService:getAll:url: " + url);
		// return this.http.get<any>( url ,this.httpOptions);

			if (isDevMode()) console.log("CRUDService:getAll url:" + url);
			var result = this.http.get<any>(url, this.httpOptions);
				return result;
		

	}
	getOne(endUrl: string, id: number) {
		//Validation
		if (endUrl === undefined || endUrl === null || endUrl.length == 0)
			throw new Error("CRUDService:getOne: endUrl was not supplied");
		if (id === undefined || id === null || id < 1)
			throw new Error("CRUDService:getOne: Id was not supplied");

		//assignment
		var url = this.baseUrl + endUrl + "/" + id;
		// if (isDevMode()) console.log("CRUDService:getOne url:" + url);
		// return this.http.get<any>(url,this.httpOptions);

	if (isDevMode()) console.log("CRUDService:getOne url:" + url);
			var result = this.http.get<any>(url, this.httpOptions);
			return result;
	
	}
	deleteOne(endUrl: string, id: any) {
		var url = this.baseUrl + endUrl;
		if (isDevMode()) console.log("CRUDService:deleteOne url:" + url);
		return this.http.delete(url, this.httpOptions);
	}
	addOne(endUrl: string, objectToPost: any) {
		var url = this.baseUrl + endUrl;
		if (isDevMode()) console.log("CRUDService:addOne url:" + url);
		console.log("pizda")
		return this.http.post<any>(url, objectToPost, this.httpOptions);
	}
	editOne(endUrl: string, objectToPut: any) {
		// Put the Id somehow
		var url = this.baseUrl + endUrl + "/" + objectToPut.Id;
		if (isDevMode()) console.log("CRUDService:editOne url:" + url);
		return this.http.put<any>(url, objectToPut, this.httpOptions);
	}
	postData(endUrl: string, objectToPut: any) {
		// Put the Id somehow
		var url = this.baseUrl + endUrl;
		if (isDevMode()) console.log("CRUDService:postData url:" + url);
		return this.http.post<any>(url, objectToPut, this.httpOptions);
	}


	
}

