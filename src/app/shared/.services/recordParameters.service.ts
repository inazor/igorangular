import { Injectable, EventEmitter } from '@angular/core';
import { IFieldParameters } from '../interfaces/fieldParameters';
import { IRecordParameters } from '../interfaces/recordParameters';

@Injectable({
  providedIn: 'root'
})
export class RecordParametersService {
    //emit p to children
    public viewParametersLoaded$ : EventEmitter<IRecordParameters>;

    constructor() { 
      this.viewParametersLoaded$ = new EventEmitter<IRecordParameters>();
    }

    public broadcast( p:IRecordParameters ): void{
      
      this.validateIRecordParameters(p);
      this.viewParametersLoaded$.emit(p);
    }

    //TODO: move to separate library
    private validateIRecordParameters(p: IRecordParameters){

      if (p===null)
        throw new Error("RecordParametersService: empty parameters received ");
  
      if ( p.RecordId === null || p.RecordId == 0 )
        throw new Error("RecordParametersService: RecordId not sent");
  
      if ( p.RecordData === null)
        throw new Error("RecordParametersService: RecordData not sent");  
        
      if ( p.ModelName == null || p.ModelName.length == 0 ) {
          throw new Error("RecordParametersService: ModelName not sent");
        }        
  
      if (p.ModelDataFormat == null) {
        throw new Error("RecordParametersService: ModelDataFormat not sent");
      }
  
      if ( p.ModelsDescription == null ) {
        throw new Error("RecordParametersService: ModelsDescription not sent");
      }
  
    }
}
