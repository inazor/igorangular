import { Injectable, EventEmitter } from '@angular/core';
import { IFieldParameters } from '../interfaces/fieldParameters';

@Injectable({
  providedIn: 'root'
})
export class FieldParametersService {
    //emit p to children
    public viewParametersLoaded$ : EventEmitter<IFieldParameters>;

    constructor() { 
      this.viewParametersLoaded$ = new EventEmitter<IFieldParameters>();
    }

    public broadcast( p:IFieldParameters ): void{
      this.validateIFieldParameters(p);
      this.viewParametersLoaded$.emit(p);
    }

    //TODO: move to separate library
    private validateIFieldParameters(p: IFieldParameters){

      if (p===null)
        throw new Error("FieldParametersService: empty parameters received ");
  
      if ( p.RecordId === null || p.RecordId == 0 )
        throw new Error("FieldParametersService: RecordId not sent");
  
       
      if ( p.ModelName == null || p.ModelName.length == 0 ) {
          throw new Error("FieldParametersService: ModelName not sent");
        }        
  
      if (p.ModelDataFormat == null) {
        throw new Error("FieldParametersService: ModelDataFormat not sent");
      }
  
      if ( p.ModelsDescription == null ) {
        throw new Error("RecordParametersService: ModelsDescription not sent");
      }
  
    }    
}
