import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'boolean-list',
  templateUrl: './boolean-list.component.html',
  styleUrls: ['./boolean-list.component.css']
})
export class BooleanListComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
