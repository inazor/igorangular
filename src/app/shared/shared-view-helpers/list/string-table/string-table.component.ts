import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'string-table',
  templateUrl: './string-table.component.html',
  styleUrls: ['./string-table.component.css']
})
export class StringTableComponent implements OnInit {
  @Input() data: string;
  constructor() { }
  ngOnInit() {
  }

}
