import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'string-list',
  templateUrl: './string-list.component.html',
  styleUrls: ['./string-list.component.css']
})
export class StringListComponent implements OnInit {
  @Input() data: string;
  constructor() { }
  ngOnInit() {
  }

}
