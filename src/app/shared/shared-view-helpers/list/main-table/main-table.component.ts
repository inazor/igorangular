import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'main-table-field',
  templateUrl: './main-table.component.html',
  styleUrls: ['./main-table.component.css']
})
export class MainTableComponent implements OnInit {
@Input() dataFormat: any;
@Input() data: any;
  constructor() { }

  ngOnInit() {
  }

}
