import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'field-list',
  templateUrl: './field-list.component.html',
  styleUrls: ['./field-list.component.css']
})
export class FieldListComponent implements OnInit {
@Input() dataFormat: any;
@Input() data: any;
  constructor() { }

  ngOnInit() {
  }

}
