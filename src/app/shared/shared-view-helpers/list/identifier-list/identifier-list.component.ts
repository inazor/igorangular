import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'identifier-list',
  templateUrl: './identifier-list.component.html',
  styleUrls: ['./identifier-list.component.css']
})
export class IdentifierListComponent implements OnInit {
  @Input() data: number;
  constructor() { }

  ngOnInit() {
  }

}
