import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'label-form',
  templateUrl: './label-form.component.html',
  styleUrls: ['./label-form.component.css']
})
export class LabelFormComponent implements OnInit {

	@Input() labelText: string;

	constructor() { }

	ngOnInit() {
	}

}
