import { Component, OnInit, Input, forwardRef, isDevMode } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator } from '@angular/forms';
import { CRUDService } from 'src/app/shared/.services/crud.service';
@Component({
	selector: 'field-form',
	templateUrl: './field-form.component.html',
	styleUrls: ['./field-form.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => FieldFormComponent),
			multi: true
		},
		{
			provide: NG_VALIDATORS,
			useExisting: forwardRef(() => FieldFormComponent),
			multi: true,
		}
	]
})
export class FieldFormComponent implements ControlValueAccessor, Validator {
	@Input() fieldName: string;
	@Input() fields: string[];
	@Input() labelWidth: number = 4;
	@Input() modelDataFormat: any; // if sent as parameter, not loaded from localstorage (for one2many lists)
	@Input() dataFormat: any; // if sent as parameter, not loaded from localstorage (for one2many lists)
	@Input() data: any; // if sent as parameter, not loaded from localstorage (for one2many lists)
	@Input() modelName: string = "";
	@Input() modelData: any;

	modelDescription: any;
	//modelDataFormat: any;

	crudServicePrefix: string;


	itemId: number = null;

	isAdd = false;;
	fieldRequired: boolean;
	fieldEditable: boolean;
	fieldDescription: string;
	recordNamesUrl: string = null;
	isInitialized = false;
	propagateChange = (_: any) => { };

	registerOnValidatorChange?(fn: () => void): void {
	}

	writeValue(str: string): void {
		if (this.data != null && this.data != undefined)
			this.sendDataToForm(this.data);
	}
	registerOnChange(fn: any): void {
		this.propagateChange = fn;
	}
	registerOnTouched(fn: any): void {
	}
	setDisabledState?(isDisabled: boolean): void {
		throw new Error("Method not implemented.");
	}
	/*      
	  Description:
	  Sends data to form in parent component
	  Called from:
	   src\app\FormComponents\main-form\main-form.component.html
	   in
	   ngOnInit()
  
	  Input: 
	  Response object
	  
	  Output:
	  None
		  */
	sendDataToForm(childData) {
		this.data = childData;
		this.propagateChange(childData);
	}

	constructor(private crudService: CRUDService
	) { }

	ngDoCheck() {
		if (!this.isInitialized) {
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit");
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit:fieldName " + this.fieldName);
			if (isDevMode()) console.log("FieldFormComponent:ngOnInit:modelData ");
			if (isDevMode()) console.log(this.modelData);


			//Validation
			if (this.fieldName === undefined || this.fieldName === null || this.fieldName.length === 0)
				throw new Error("FieldFormComponent: Required parameter 'name' not given");

			//assignment
			if (this.modelDataFormat != undefined || JSON.parse(localStorage.getItem('AutomaticCard_modelDataFormat'))) {
				if (isDevMode()) console.log("FieldFormComponent:ngOnInit:modelDataFormat ");
				if (isDevMode()) console.log(this.modelDataFormat);


				if (this.modelDataFormat === undefined)
					this.modelDataFormat = JSON.parse(localStorage.getItem('AutomaticCard_modelDataFormat'));

				if (this.modelDataFormat == null || this.modelDataFormat == undefined || !this.modelDataFormat.hasOwnProperty(this.fieldName)) {
					console.log(this.modelDataFormat);
					throw new Error("FieldFormComponent:ngDoCheck: Modeldataformat does not contain key " + this.fieldName);
				}

				this.dataFormat = this.modelDataFormat[this.fieldName];
				if (this.modelName === undefined)
					this.modelName = localStorage.getItem('AutomaticCard_modelName');
				if (this.modelData === undefined && localStorage.getItem('AutomaticCard_modelData'))
					this.modelData = JSON.parse(localStorage.getItem('AutomaticCard_modelData'));

				this.itemId = +localStorage.getItem('AutomaticCard_itemId');
				this.fieldRequired = this.dataFormat.hasOwnProperty("Required") ? this.dataFormat.Required : true;
				this.fieldEditable = this.dataFormat.hasOwnProperty("Editable") ? this.dataFormat.Editable : false;
				this.fieldDescription = this.dataFormat.hasOwnProperty("Description") ? this.dataFormat.Description : this.fieldName;
				this.modelDescription = JSON.parse(localStorage.getItem('AutomaticCard_modelDescription'));
				if (this.dataFormat.hasOwnProperty("RelatedModel")) {
					this.modelDescription = this.modelDescription[this.dataFormat.RelatedModel];
				}
				if (localStorage.getItem('automaticList_selectedItemId') && localStorage.getItem('AutomaticCard_modelData')) {
					console.log("pokaz mnie! ",this.modelData[this.fieldName] )
					this.data = this.modelData[this.fieldName];
					this.isInitialized = true;
					this.sendDataToForm(this.data);
				}
			}
		}

	}

	validate() {
		if (this.dataFormat != undefined) {
			if ((this.data != null && this.data != undefined && this.data != "") || !this.dataFormat.Required) {
				return null;
			} else {
				return { 'valid': false };
			}
		}
	}
}

