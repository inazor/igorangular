import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { CRUDService } from '../../../.services/crud.service';

@Component({
  selector: 'many2one-form',
  templateUrl: './many2oneform.component.html',
  styleUrls: ['./many2oneform.component.css']
})
export class Many2oneformComponent implements OnInit {
  /*
    Behaviour:
    Display data:
    if it is editable - Dropdown is shown with list of all Users
    If it is not editable - Onlx data from  CeratedByUser is shown
  
    Send data:
    If nothing is selected:
    CreatedByUserId is sent as  null
  
    If something is selected from dropdown
    CreatedByUserId is sent as User Id
  
  
    Data Definition to receive from server
    "CreatedByUser": {
        "DataType": "Many2One",
        "ForeignKey": "CreatedByUserId",
        "Description": "Created By",
        "RelatedModel": "User", // needed to sreate list in combo box
        "Required": true,
        "Editable": true
      },
  
    Data to receive from server
      "CreatedByUser": {5,"Ivo Ivić"}
  
    Data to send to server  
      "CreatedByUserId": 5
  
  */
  @Input() recordNamesUrl: string;
  @Input() data: any;
  @Input() isRequired: boolean;
  @Input() isEditable: boolean = true;
  @Output() inputData = new EventEmitter<number>();

  selectedData: any = null;
  recordNames: SelectItem[] = [];
  constructor(private crudService: CRUDService) { }

  ngOnInit(): void {
    console.log("czy jestem editable",this.isEditable,this.recordNamesUrl,this.data,this.isRequired);
    if(this.isEditable){
      console.log("hejka jestem editable",this.data);
    this.crudService.getRecordNames(this.recordNamesUrl).subscribe(response => {
      response.Data.forEach(element => {
        this.recordNames.push({value: element[0],label: element[1]});
      })
      
      if(this.data != null && this.data != undefined){
      this.selectedData=this.data[0];
      this.sendDataToParent();
      }
    })
  }
  else
  {
    console.log("hejka jestem nieEditable",this.data);
    if(this.data != null && this.data != undefined){
    this.selectedData=this.data[1];
    this.inputData.emit(this.data[0]);
    }
  }


  }
  sendDataToParent() {
    this.inputData.emit(this.selectedData);
  }
}
