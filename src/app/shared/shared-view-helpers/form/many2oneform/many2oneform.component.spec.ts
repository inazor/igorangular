import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Many2oneformComponent } from './many2oneform.component';

describe('Many2oneformComponent', () => {
  let component: Many2oneformComponent;
  let fixture: ComponentFixture<Many2oneformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Many2oneformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Many2oneformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
