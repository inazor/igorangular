import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Many2manyformComponent } from './many2manyform.component';

describe('Many2manyformComponent', () => {
  let component: Many2manyformComponent;
  let fixture: ComponentFixture<Many2manyformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Many2manyformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Many2manyformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
