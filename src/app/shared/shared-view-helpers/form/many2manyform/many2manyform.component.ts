import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { CRUDService } from 'src/app/shared/.services/crud.service';
import { MultiSelect } from 'primeng/primeng';

@Component({
  selector: 'many2many-form',
  templateUrl: './many2manyform.component.html',
  styleUrls: ['./many2manyform.component.css']
})

export class Many2manyformComponent implements OnInit {

  @Input() recordNamesUrl: string;
  @Input() data: any[];
  @Input() isRequired: boolean;
  @Input() isEditable: boolean = true;
  @Output() inputData = new EventEmitter<number[]>();

  selectedData: number[] = [];
  notEditableData: any[] = [];
  recordNames: SelectItem[] = [];
  constructor(private crudService: CRUDService) { }
  @ViewChild('multiSelect',{static: false}) multi: MultiSelect;
ngAfterViewInit(){
}
  ngOnInit(): void {
    // if (this.data != null && this.data != undefined) {
    //   console.log("chuj");
    //   this.data.forEach(element => {
    //     this.selectedData.push(element[0]);
    //   });
    //   this.sendDataToParent();
    // }
    if (this.isEditable) {
      this.crudService.getRecordNames(this.recordNamesUrl).subscribe(response => {
        response.Data.forEach(element => {
          this.recordNames.push({ value: element[0], label: element[1] });
        })

        if (this.data != null && this.data != undefined) {
          this.data.forEach(element => {
            this.selectedData.push(element[0]);
          });
          this.sendDataToParent();
        }
        this.multi.updateLabel();
      }
      )
    }
    else {
      if (this.data != null && this.data != undefined) {
        this.data.forEach(element => {
          this.selectedData.push(element[1]);
          this.notEditableData.push(element[1]);
        }); 
        this.inputData.emit(this.notEditableData);
      }
    }


  }
  sendDataToParent() {
    this.inputData.emit(this.selectedData);
  }
}