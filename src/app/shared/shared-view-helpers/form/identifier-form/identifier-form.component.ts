import { Component, Input} from '@angular/core';

@Component({
  selector: 'identifier-form',
  templateUrl: './identifier-form.component.html',
  styleUrls: ['./identifier-form.component.css']
})
export class IdentifierFormComponent  {
  @Input() data: number = null;
  constructor() { }

}
