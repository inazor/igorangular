import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'date-form',
  templateUrl: './date-form.component.html',
  styleUrls: ['./date-form.component.css']
})
export class DateFormComponent {
  @Input() isRequired: boolean;
  @Input() isEditable: boolean;
  @Input() data: any;
  @Output() sendData = new EventEmitter<any>();

  constructor() { }

  sendDataToParent() {
    this.sendData.emit(this.data);
  }

}
