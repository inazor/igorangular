import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'boolean-form',
  templateUrl: './boolean-form.component.html',
  styleUrls: ['./boolean-form.component.css']
})
export class BooleanFormComponent implements OnInit {


  @Input() isRequired: boolean = false;
  @Input() isEditable: boolean = false;
  @Input() data: boolean = null;
  @Output() inputData = new EventEmitter<boolean>();

  constructor() { }
  ngOnInit() {
    
  }

  sendDataToParent() {
    this.inputData.emit(this.data);
  }

}
