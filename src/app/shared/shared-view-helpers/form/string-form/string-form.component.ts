import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'string-form',
  templateUrl: './string-form.component.html',
  styleUrls: ['./string-form.component.css']
})
export class StringFormComponent {

  @Input() isRequired: boolean = false;
  @Input() data: string = null;
  @Output() inputData = new EventEmitter<string>();

  constructor() { }

  sendDataToParent() {
    this.inputData.emit(this.data);
  }
}
