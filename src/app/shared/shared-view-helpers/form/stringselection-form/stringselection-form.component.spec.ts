import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StringselectionFormComponent } from './stringselection-form.component';

describe('StringselectionFormComponent', () => {
  let component: StringselectionFormComponent;
  let fixture: ComponentFixture<StringselectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StringselectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StringselectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
