import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'stringselection-form',
  templateUrl: './stringselection-form.component.html',
  styleUrls: ['./stringselection-form.component.css']
})
export class StringselectionFormComponent implements OnInit {

  @Input() data: string[];
  @Input() isRequired: boolean;
  @Input() isEditable: boolean = true;
  @Input() selectedData: string;
  @Output() inputData = new EventEmitter<string>();
  records: SelectItem[] = [];
  constructor() { }

  ngOnInit(): void {
    console.log(this.data);
    if (this.isEditable) {
          this.data.forEach(singleData => this.records.push({label: singleData, value: singleData}));
    }
    else {

    }

  }
  sendDataToParent() {
    this.inputData.emit(this.selectedData);
  }
}
