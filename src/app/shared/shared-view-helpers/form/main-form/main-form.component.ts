import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator } from '@angular/forms';

@Component({
  selector: 'main-form-field',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MainFormComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MainFormComponent),
      multi: true,
    }
  ]
})
export class MainFormComponent implements ControlValueAccessor, Validator, OnInit {
  @Input() dataFormat: any;
  @Input() col: string;
  @Input() data: any;
  @Input() description: any;
  propagateChange = (_: any) => { };

  registerOnValidatorChange?(fn: () => void): void {
  }

  writeValue(str: string): void {
    this.data = str;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
  }
  setDisabledState?(isDisabled: boolean): void {
    throw new Error("Method not implemented.");
  }
       /*      
         Description:
         Sends data to form in parent component
         Called from:
          src\app\FormComponents\main-form\main-form.component.html
          in
          ngOnInit()

         Input: 
         Response object
         
         Output:
         None
             */
  sendDataToForm(childData) {
    this.data = childData;
    this.propagateChange(childData);
  }

  constructor(
  ) { }

  ngOnInit() {
  }
  validate() {

    if ((this.data != null && this.data != undefined && this.data != "") || !this.dataFormat.Required) {
      return null;
    } else {
      return { 'valid': false };
    }
  }

}
