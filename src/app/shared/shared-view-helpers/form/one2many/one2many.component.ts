import { Component, Input, forwardRef, EventEmitter, Output, isDevMode } from '@angular/core';
@Component({
  selector: 'one2many-form',
  templateUrl: './one2many.component.html',
  styleUrls: ['./one2many.component.css']
})
export class One2manyFormComponent {
  @Input() fields: string[];
  @Input() dataFormat: any;
  @Input() isEditable: boolean = true;
  @Input() fieldName: string;
  @Input() data: any;
  
  line: any;
  lines: any;
  
  
  //data=['chuj','cipa'];
  constructor() { }

  ngOnInit(): void {
    
		if (isDevMode()) console.log("One2manyFormComponent:ngOnInit");
		if (isDevMode()) console.log("One2manyFormComponent:ngOnInit:dataFormat");
		if (isDevMode()) console.log(this.dataFormat);
		
  }



  }

