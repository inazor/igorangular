import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { One2manyFormComponent } from './one2many.component';

describe('One2manyComponent', () => {
  let component: One2manyFormComponent;
  let fixture: ComponentFixture<One2manyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ One2manyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(One2manyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
