import { IRecordParameters } from '../interfaces/recordParameters';

export class Validators{
   constructor(){}

   private validateIRecordParameters(p: IRecordParameters){

    if (p===null)
      throw new Error("RecordParametersService: empty parameters received ");

    if ( p.RecordId === null || p.RecordId == 0 )
      throw new Error("RecordParametersService: RecordId not sent");

    if ( p.RecordData === null)
      throw new Error("RecordParametersService: RecordData not sent");  
      
    if ( p.ModelName == null || p.ModelName.length == 0 ) {
        throw new Error("RecordParametersService: ModelName not sent");
      }        

    if (p.ModelDataFormat == null) {
      throw new Error("RecordParametersService: ModelDataFormat not sent");
    }

    if ( p.ModelsDescription == null ) {
      throw new Error("RecordParametersService: ModelsDescription not sent");
    }

  }
    
}