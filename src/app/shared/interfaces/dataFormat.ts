export interface IDataFormat {
    DataType : string,
    Description : string, 
    Required: boolean,
    Editable: boolean,

    Model?: string,
    DefaultValue? : any,

    RelatedModel? : string,
    ForeignKey?: string,
    RelatedModelForeignKey? : string,
    DataFormat? : any

}
