import { IDataFormat } from './dataFormat';

export interface IModelDetails {
    Name : string, 
    NamePlural : string,
    DataFormat : {[fieldName: string]: IDataFormat}
}
