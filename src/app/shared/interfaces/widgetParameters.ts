import { IDataFormat } from './dataFormat';
import { IModelDescription } from './modelDescription';
import { EViewLayout } from '../enums/viewLayout.enum';
import { IFieldDataFormat } from './fieldDataFormat';

export interface IWidgetParameters {
    FieldName : string,
    FieldDataFormat : IFieldDataFormat,
    FieldValue: any,

    RecordId: number,
    RecordData: {[fieldName: string]: any}    

    ModelName : string,
    ModelDataFormat: {[fieldName: string]: IFieldDataFormat},
    ModelData:  [{[fieldName: string]: any}],

    ModelsDescription : {[modelName: string]: IModelDescription},

    ViewLayout: EViewLayout,
}
