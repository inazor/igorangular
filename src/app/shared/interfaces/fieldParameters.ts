import { IModelDescription } from './modelDescription';
import { IDataFormat } from './dataFormat';
import { EViewLayout } from '../enums/viewLayout.enum';
import { IFieldDataFormat } from './fieldDataFormat';


export interface IFieldParameters {
    FieldName : string,
    FieldDataFormat : IFieldDataFormat,
    FieldData: any,

    RecordId: number,

    ModelName : string,
    ModelDataFormat: {[fieldName: string]: IFieldDataFormat},
    ModelData:  [{[fieldName: string]: any}],

    ModelsDescription : {[modelName: string]: IModelDescription},

    ViewLayout: EViewLayout,
}
