import { IModelDescription } from './modelDescription';
import { IDataFormat } from './dataFormat';
import { EViewLayout } from '../enums/viewLayout.enum';
import { IFieldDataFormat } from './fieldDataFormat';


export interface IRecordParameters {
    RecordId: number,
    RecordData: {[fieldName: string]: any}    

    ModelName : string,
    ModelDataFormat: {[fieldName: string]: IFieldDataFormat},

    ModelsDescription : {[modelName: string]: IModelDescription},

    ViewLayout: EViewLayout,
}
